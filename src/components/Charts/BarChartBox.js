import { Bar, BarChart, ResponsiveContainer, Tooltip } from 'recharts';

const BarChartBox = (props)=>{
    
    return(
    <div className='barChartBox'>
        <h4>{props.title}</h4>
        <div className="chart">
            <ResponsiveContainer width="100%" height={150}>
                <BarChart data={props.chartData}>
                    <Tooltip
                        contentStyle={{backgroundColor:"#2a3447", borderRadius:"5px"}}
                        labelStyle={{display:"none"}}
                        cursor={{fill:"none"}}
                    />
                    <Bar dataKey={props.dataKey} fill={props.color} />
                </BarChart>
            </ResponsiveContainer>
        </div>
    </div>);
}

export default BarChartBox;