import { Cell, Pie, PieChart, ResponsiveContainer, Tooltip } from 'recharts';

const data =[
    {name: "Mobile", value: 400, color:"#0088fe"},
    {name: "Desktop", value: 300, color:"#00c49f"},
    {name: "Laptop", value: 300, color:"#ffbb28"},
    {name: "Tablet", value: 200, color:"#ff8042"}
];
const PieChartBox = ()=>{
    return(
    <div className='pieChartBox'>
         <h4>Pistes par source</h4>
         <div className="chart">
            <ResponsiveContainer width="99%" height={400}>
                <PieChart>
                    <Tooltip contentStyle={{backgroundColor:"white", borderRadius:"5px"}}/>
                    <Pie
                        data={data}
                        innerRadius={"70%"}
                        outerRadius={"90%"}
                        fill='#8884d8'
                        paddingAngle={5}
                        dataKey="value"
                    >
                        {data.map((item)=>{
                            return(<Cell key={item.name} fill={item.color}/>);
                        })}
                    </Pie>
                </PieChart>
            </ResponsiveContainer>
         </div>
         <div className="options">
                {data.map((item)=>{
                    return(
                    <div className='option' key={item.name}>
                        <div className="title">
                            <div className="dot" style={{backgroundColor:item.color}}></div>
                            <span>{item.name}</span>
                        </div>
                        <span>{item.value}</span>
                    </div>);
                })}
         </div>
    </div>);
}
export default PieChartBox;