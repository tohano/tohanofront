import {topDealUsers} from '@/data/data';

const TopBox = ()=>{
    const users = topDealUsers;
    return(<div className='topBox'>
        <h4>Meilleure association</h4>
        <div className="list">
            {users.map((user)=>{
                return(
                    <div className="listItem" key={user.id}>
                        <div className="user">
                            <img src={user.img} alt="" />
                            <div className="userTexts">
                                <span className="userName">{user.username}</span>
                                <span className="email">{user.email}</span>
                            </div>
                        </div>
                        <span className="amount">${user.amount}</span>
                    </div>
                );
            })}
        </div>
    </div>);
}

export default TopBox;