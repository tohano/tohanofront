import React, { useRef } from "react";
import axios from "axios";
import Link  from "next/link";
import { useRouter } from "next/router";
import { Col, Container, Row, Toast } from "react-bootstrap";
import Title from "../Reuseable/Title";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const notify = () => {
        toast.error('Email ou mot de passe incorrect', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "colored",
          });
    };

    

function UserAuthenticationForm(){
    
    const mailRef = useRef('');
    const passwordRef = useRef('');
    const router = useRouter();

    const  handleUserAuthentication = (event)=>{
        event.preventDefault();
        let currentUser = {
            mail: mailRef.current.value,
            motDePasse: passwordRef.current.value
        }
        // const form = new FormData();
        // form.append("user", currentUser)
        //alert(currentUser.motDePasse);
        connectUser(currentUser);
    }
    
    const  connectUser=(currentUser)=>{
        // alert(user.mail + "; " + user.motDePasse);
        
        // form.append("mail", mailRef.current.value);
        // form.append("motDePasse", passwordRef.current.value);
        //alert(currentUser.motDePasse);
        axios.post("http://localhost:8080/connexion", currentUser)
            .then((response) =>{
                console.log(response.data);
                const connectedUser= response.data;
                console.log(connectedUser);
                switch(connectedUser.userType){
                    case "malade":
                        sessionStorage.setItem("connectedUser",JSON.stringify(connectedUser));
                        router.push("/maladeHome");
                        break;
                     case "donateur":
                        sessionStorage.setItem("connectedUser",JSON.stringify(connectedUser));
                        router.push("/donateurHome");
                        break;
                    default:
                        notify()

                }
            }).catch(function(error) {
                // console.error("erreur venant de UserAuthenticationForm ::: " + error.message);
            });
    }

    return (
        <section className="contact-form-area mt-150">
            <Container>
                <Row className="justify-content-center">
                    <Col lg={8}>
                        <Title title="vous pouvez vous connecter" tagline="Avec votre mail et votre mot de passe," className="text-center" />
                    </Col>
                </Row>
                <Row className="justify-content-center">
                    <Col lg={8}>
                    <form onSubmit={handleUserAuthentication}>
                        <div className="conact-form-item">
                            <Row>
                            <Col lg={6} md={6}>
                                <div className="input-box mt-20">
                                <h5 className="text-orange">Votre e-mail</h5>
                                <input name="userMail" ref={mailRef} id="userMail" type="email" placeholder="Enter votre mail ici" size="150" pattern="[a-zA-Z0-9._\-]+@[a-zA-Z0-9._\-]+.[a-zA-Z.]{2,15}" required/>
                                </div>
                            </Col>
                            <Col lg={6} md={6}>
                                <div className="input-box mt-20">
                                    <h5 className="text-orange">Votre mot de passe</h5>
                                    <input name="userPassword" id="userPassword" ref={passwordRef} type="password" size="255" placeholder="Enter votre mot de passe ici" />
                                </div>
                            </Col>
                            
                            <Col lg={12}>
                                <div className="input-box mt-20 text-center">
                                    <button className="main-btn main-btn-2" type="reset">
                                        Réinitialiser
                                    </button>
                                    <button className="main-btn ml-100" type="submit">
                                        Se connecter
                                    </button>
                                </div>
                            </Col>
                            </Row>
                        </div>
                        </form>
                        <p className="ml-140 mt-30">Ou si vous n'avez pas de compte, <Link href="/signinPatient">veuillez-vous inscrire</Link></p>
                    </Col>
                </Row>
                    <ToastContainer
                        position="top-right"
                        autoClose={5000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                        theme="colored"
                        />
                
            </Container>
        </section>
    )
}

export default UserAuthenticationForm;
