import React from 'react';
// TSY MIASA FA SAO HILAINA AMIN ILAY FICHE MEDICALE 
export default function Step3({ formData, setFormData, handleFormSubmit, handlePrevious }) {
  const { nomMaladie, description, dateDebut, dateDerniereConsult, statut, medecin, onm } = formData;

  const handleChange = (e) => {
    //const { name, checked } = e.target;
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  return (
    <div>
      <h2>Étape 3</h2>
      <label htmlFor="nomMaladie">Nom de la maladie :</label>
      <input
        type="text"
        id="nomMaladie"
        name="nomMaladie"
        value={nomMaladie}
        onChange={handleChange}
      />
      <label htmlFor="description">Description :</label>
      <textarea
        id="description"
        name="description"
        value={description}
        onChange={handleChange}
      ></textarea>
      <label htmlFor="dateDebut">Date de début :</label>
      <input
        type="date"
        id="dateDebut"
        name="dateDebut"
        value={dateDebut}
        onChange={handleChange}
      />
      <label htmlFor="dateDerniereConsult">Date de dernière consultation :</label>
      <input
        type="date"
        id="dateDerniereConsult"
        name="dateDerniereConsult"
        value={dateDerniereConsult}
        onChange={handleChange}
      />
      <label>
        <input
          type="checkbox"
          name="statut"
          checked={statut}
          onChange={handleChange}
        />
        Urgent
      </label>
      <label htmlFor="medecin">Médecin :</label>
      <input
        type="text"
        id="medecin"
        name="medecin"
        value={medecin}
        onChange={handleChange}
      />
      <label htmlFor="onm">ONM :</label>
      <input
        type="text"
        id="onm"
        name="onm"
        value={onm}
        onChange={handleChange}
      />
      <button onClick={handlePrevious}>Précédent</button>
      <button onClick={handleFormSubmit}>Soumettre</button>
    </div>
  );
}