import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import { useState } from "react";
import axios from 'axios';
import { useRouter } from 'next/router';
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";

const notify = () => {
        toast.success('Inscription réussie, vous serez redirigé automatiquement dans 3 secondes', {
          position: "top-right",
          autoClose: 3500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "colored",
          });
    };
const notifyError = () => {
  toast.error('Cet email est déjà utilisé, veuillez-vous connecter', {
    position: "top-right",
    autoClose: 3500,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
  });
};
export default function Formulaire() {
  const router = useRouter();
  const [currentStep, setCurrentStep] = useState(1);
  const [formData, setFormData] = useState({
    adresse: '',
  });

  

  const handleNext = () => {
    
    setCurrentStep(currentStep + 1);
    // console.log('Données du formulaire 1 soumises :', formData);
  };

  const handlePrevious = () => {
    setCurrentStep(currentStep - 1);
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    let idUser = await axios.post("http://localhost:8080/inscritpion/malade",formData)
    .then(function (response) {
        // console.log("Reponse data post io : " + response.data)
        // console.log(typeof(response.data))
        // Stockage des données dans sessionStorage à tester
        let user = {id:response.data,userType:'malade'};
        notify()
        // console.log(user)
        if (typeof window !== 'undefined') {
      // Vérification de la disponibilité de sessionStorage
      sessionStorage.setItem('connectedUser', JSON.stringify(user));
    }
    setTimeout(()=>{router.push('/maladeHome')},3500)
        

    }).catch(function (error){
        // console.error("Misy diso ooo " + error)
        notifyError(error)
        router.push("/login")
    });
    
  };

  const renderStep = () => {
    switch (currentStep) {
      case 1:
        return (
            
          <Step1
            formData={formData}
            setFormData={setFormData}
            handleNext={handleNext}
          />
          
        );
      case 2:
        return (
          <Step2
            formData={formData}
            setFormData={setFormData}
            handleNext={handleNext}
            handlePrevious={handlePrevious}
          />
        );
      case 3:
        return (
          <Step3
            formData={formData}
            handleFormSubmit={handleFormSubmit}
            handlePrevious={handlePrevious}
          />
        );
      default:
        return null;
    }
  };

  return (
    <form id="inscriptionMalade" className="contact-form-area mt-100" onSubmit={handleFormSubmit}>
      {renderStep()}
        <ToastContainer
            position="top-right"
            autoClose={3500}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="colored"
            />
    </form>
  );
}
