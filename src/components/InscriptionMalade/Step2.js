import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Title from "../Reuseable/Title";
import Link from "next/link";

export default function Step2({ formData, setFormData, handleFormSubmit, handlePrevious }) {
  const { nomMalade,prenomMalade,dateNaissance,contact,adresse,cinMalade } = formData;

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const validateStep2 = () => {
    if(!nomMalade || !prenomMalade || !dateNaissance || !contact || !adresse || !cinMalade ) {
      return false;
    }
    return true;
  }

  return (
    <section className="contact-form-area">
      <Container>
        <Row className="justify-content-center">
          <Col lg={8}>
            <Title title="Informations personnelles" tagline="Etape 2/2 inscription malade" className="text-center" />
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col lg={8}>
            
              <div className="conact-form-item">
                <Row>
                    {/* eto voloany */}
                    <Col lg={6} md={6}>
                      <div className="input-box mt-20">
                          <h5 className="text-orange">Nom</h5>
                          <input
                            type="text"
                            name="nomMalade"
                            pattern="[A-Za-z]+"
                            placeholder="Nom"
                            value={nomMalade}
                            onChange={ e => handleChange(e) }
                          />
                      </div>
                    </Col>
                    <Col lg={6} md={6}>
                      <div className="input-box mt-20">
                          <h5 className="text-orange">Prénom</h5>
                          <input
                            type="text"
                            name="prenomMalade"
                            pattern="[A-Za-z]+"
                            placeholder="Prénom"
                            value={prenomMalade}
                            onChange={ e => handleChange(e) }
                          />
                      </div>
                    </Col>
                  {/* eto farany */}
                  <Col lg={6} md={6}>
                      <div className="input-box mt-20">
                          <h5 className="text-orange">Date de naissance</h5>
                          <input
                            type="date"
                            name="dateNaissance"
                            value={dateNaissance}
                            onChange={handleChange}
                          />
                      </div>
                  </Col>
                  <Col lg={6} md={6}>
                      <div className="input-box mt-20">
                          <h5 className="text-orange">Contact</h5>
                          <input
                            type="text"
                            id="contact"
                            name="contact"
                            value={contact}
                            placeholder="Contact"
                            onChange={handleChange}
                          />
                      </div>
                    </Col>
                    <Col lg={6} md={6}>
                      <div className="input-box mt-20">
                          <h5 className="text-orange">Adresse</h5>
                          <input
                            type="text"
                            id="adresse"
                            name="adresse"
                            value={adresse}
                            placeholder="Adresse"
                            onChange={handleChange}
                          />
                      </div>
                  </Col>
                  <Col lg={6} md={6}>
                      <div className="input-box mt-20">
                          <h5 className="text-orange">CIN</h5>
                          <input
                            type="text"
                            inputMode='numeric'
                            //pattern="[0-9\s]{13,19}"
                            id="cinMalade"
                            name="cinMalade"
                            value={cinMalade}
                            placeholder="CIN : XXX XXX XXX XXX"
                            maxLength={12}
                            onChange={handleChange}
                          />
                      </div>
                  </Col>
                  <Col lg={12}> 
                    {/* <Col lg={6}></Col> */}
                  
                    <div className="input-box mt-20 text-center">
                      <button onClick={handlePrevious} className="main-btn main-btn-2">Précédent</button>

                      {
                        validateStep2() ? (
                            <button className="main-btn ml-120" type="submit">
                              Valider
                            </button>
                            ) : (
                            <button className="main-btn ml-120" type="submit" disabled>
                              Valider
                            </button>)
                      }
                      
                      <p className="text-center mt-4">Vous avez déjà un compte? Connectez-vous <Link href="/login" className="nav-link">ici</Link></p>

                    </div>
                  </Col>
                </Row>
              </div>
            
            <p className="form-message"></p>
          </Col>
        </Row>
      </Container>
    </section>   
  );
}