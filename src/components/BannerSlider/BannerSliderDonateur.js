import { bannerDonateur } from "@/data/bannerDonateur";
import React from "react";
import SwiperCore, { Autoplay, EffectFade, Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import SingleBannerDonateur from "./SingleBannerDonateur";
import { style } from "motion";

SwiperCore.use([EffectFade, Navigation, Autoplay]);

const options = {
  slidesPerView: 1,
  loop: true,
  effect: "fade",
  navigation: {
    nextEl: "#main-slider__swiper-button-next",
    prevEl: "#main-slider__swiper-button-prev",
  },
  autoplay: {
    delay: 5000,
  },
};

const BannerSliderDonateur = ({ className = "", isBannerTwo = false, isBannerThree = false }) => {
  const banners = bannerDonateur;

  return (
    <section className={`banner-slider ${className}`}>
      <Swiper {...options}>
        <div className="swiper-wrapper" style={{fontFamily:'poppins', width:'auto'}}>
          {banners.map((singleSlide) => (
            <SwiperSlide key={singleSlide.id}>
              <SingleBannerDonateur singleSlide={singleSlide} isBannerTwo={isBannerTwo} isBannerThree={isBannerThree} />
            </SwiperSlide>
          ))}
        </div>
        <div>
          <span className="prev slick-arrow" id="main-slider__swiper-button-prev">
            <i className="flaticon-back"></i>
          </span>
          <div className="next slick-arrow" id="main-slider__swiper-button-next">
            <i className="flaticon-next"></i>
          </div>
        </div>
      </Swiper>
    </section>
  );
};

export default BannerSliderDonateur;
