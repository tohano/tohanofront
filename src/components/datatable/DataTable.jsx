import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import { Link } from 'react-router-dom';

const DataTable =(props)=>{
  
/*   const rows=[
    {id:1, lastName:"Snow", firstName:"Jhon", age:35, verified:true},
    {id:2, lastName:"Snow", firstName:"Jhon", age:35, img:"/assets/userrows/8.webp"},
    {id:3, lastName:"James", firstName:"Jhon", age:35},
    {id:4, lastName:"Snow", firstName:"Mickael", age:35},
    {id:5, lastName:"Eminen", firstName:"Jhon", age:35},
    {id:6, lastName:"Snow", firstName:"Jhon", age:35},
    {id:7, lastName:"Snow", firstName:"Jhon", age:35},
    {id:8, lastName:"Snow", firstName:"Lincon", age:35},
    {id:9, lastName:"Snow", firstName:"Jhon", age:35}
  ]; */
  const handleDelete = (id)=>{
    //delete the item
    console.log(id + "has been deleted");
  }
  const actionColumn={
    field:"action",
    headerName: "Action",
    width:90,
    renderCell:(params)=>{
      return(
        <div className="action">
          <Link to={`/${props.slug}/${params.row.id}`}>
            <img src="/assets/view.svg" alt="" />
          </Link>
          <div className="delete" onClick={()=>handleDelete(params.row.id)}>
            <img src="/assets/delete.svg" alt="" />
          </div>
        </div>
      );
    }
  }
    return(
      <div className="dataTable">
        <DataGrid className='dataGrid'
          rows={props.rows}
          columns={[...props.columns, actionColumn]}
          initialState={{
            pagination:{
              paginationModel:{
                pageSize:8,
              },
            },
          }}
          slots={{toolbar:GridToolbar}}
          slotProps={{
            toolbar: {//pour la recherche
              showQuickFilter: true,
              quickFilterProps: { debounceMs: 500 },
            },
          }}
          pageSizeOptions={[5]}
          checkboxSelection
          disableRowSelectionOnClick
          disableColumnFilter
          disableDensitySelector
          disableColumnSelector
        />
      </div>
      );
}
export default DataTable;