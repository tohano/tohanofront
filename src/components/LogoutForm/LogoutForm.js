import { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import React from "react";
import { useRouter } from "next/router";

const LogoutForm = () => {
    const [connectedUser, setConnectedUser] = useState({id:0,userType:"internaute"});
    const [logout, setLogout] = useState(false);
    const route = useRouter();

    function proceedLogout(){
        setConnectedUser(sessionStorage.setItem('connectedUser','{id:0,userType:"internaute"}'));
        const route = useRouter();
        route.push("/");
    }

    function cancelLogout(){
        setConnectedUser(sessionStorage.getItem("connectedUser"));
        switch(connectedUser.userType){
            case "malade":
                route.push("/maladeHome");
                break;
            case "donateur":
                route.push("/donateur");
                break;
            case "moderateur":
                Troute.push("/admin-home");
                break;
            default:
                route.push("/");
        }
    }

    useEffect(() => {
        if(logout){
            proceedLogout();
        }else{
            cancelLogout();
        }
    }, [logout]);

    function handleLogout(choice){
        setLogout(choice);
    }

    return (
        <Container>
            <Row className="justify-content-center align-item-center">
                <Col className="text-align-center">
                    <form>
                        <Row>
                            <h4>Êtes-vous sur de vouloir vous déconnecter?</h4>
                        </Row>
                        <Row>
                            <Button onClick={()=>setLogout(true)}>Oui</Button>
                            <Button>Non</Button>
                        </Row>
                    </form>
                </Col>
            </Row>
        </Container>
    )
}

export default LogoutForm;