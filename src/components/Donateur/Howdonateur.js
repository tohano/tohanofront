import { howdonateur } from "@/data/howdonateur";
import React from "react";
import { Col, Container, Image, Row } from "react-bootstrap";

const { title, tagline, title2, items } = howdonateur;

const Item = ({ item = {} }) => {
  const { title, text } = item;
  return (
    <div className="item mt-50">
      <i className="flaticon-checkmark"></i>
      <h4 className="tohano-a">{title}</h4>
      <p className="tohano-text">{text}</p>
    </div>
  );
};



const Howdonateur = () => {
  return (
    <section className="why-choose-area" style={{ margin: '100px 50px'}}>
      <Container>
        
            <div className="why-choose-content">
              <span>{tagline}</span>
              <h3 className="title">{title2}</h3>
              {items.map((item) => (
                <Item key={item.id} item={item} />
              ))}
            </div>
          
      </Container>
    </section>
  );
};

export default Howdonateur;
