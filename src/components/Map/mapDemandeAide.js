import { MapContainer, TileLayer, Marker, Popup, useMap } from "react-leaflet";
import { Icon } from "leaflet";
import "leaflet/dist/leaflet.css";
import { useEffect } from "react";
import Link from "next/link";
const MapDemande = ({ coords, markers }) => {

  const customIcon = new Icon({
    iconUrl: "https://cdn-icons-png.flaticon.com/512/447/447031.png",
    iconSize: [38, 38],
  });

  const Recenter = ({coords}) => {
    const map = useMap();
    useEffect(() => {
        map.setView(coords,13);
    }, [coords]);
    return null;
}

  return (
    <div className="content">
      <MapContainer center={coords} zoom={13} scrollWheelZoom={true}>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">Tohano</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />

        {markers.map((marqueur) => {
          return (
            <Marker
              position={[marqueur.latitude, marqueur.longitude]}
              icon={customIcon} key={marqueur.idDemandeAide}
            >
              <Popup>
                <Link href={`/details-demande-aide/${marqueur.idDemandeAide}`} passHref>Voir détails</Link>
              </Popup>
            </Marker>
          );
        })}
        <Recenter coords={coords}/>
      </MapContainer>
    </div>
  );
};

export default MapDemande;