import React, { useEffect, useState } from "react";
import axios from 'axios';

const FactureComponent = () => {
    const [malades, setMalades] = useState([]);
    const [selectedMalade, setSelectedMalade] = useState(null);
    const [facture, setFacture] = useState(null);
    const [showPopup, setShowPopup] = useState(false);

    const fetchDonations = async () => {
        try {
            // Récupérer l'ID de l'utilisateur connecté depuis votre système d'authentification
            const userId = "1"; // Remplacez par la méthode appropriée pour récupérer l'ID de l'utilisateur

            const response = await axios.get(`http://localhost:8080/listpatient?userId=${userId}`);
            const maladesData = response.data;
            setMalades(maladesData);
        } catch (error) {
            console.error(error);
        }
    };

    const fetchFacture = async () => {
        try {
            const response = await axios.get('http://localhost:3000/generate-invoice');
            const factureData = response.data;
            setFacture(factureData);
        } catch (error) {
            console.error(error);
        }
    };

    const imprimerFacture = () => {
        const elementFacture = document.getElementById('facture');
        window.print();
    };

    const telechargerFacture = () => {
        const elementFacture = document.getElementById('facture');

        const blob = new Blob([elementFacture.outerHTML], { type: 'application/pdf' });

        const url = URL.createObjectURL(blob);

        const lienTelechargement = document.createElement('a');
        lienTelechargement.href = url;
        lienTelechargement.download = 'facture.pdf';

        document.body.appendChild(lienTelechargement);
        lienTelechargement.click();

        URL.revokeObjectURL(url);
        document.body.removeChild(lienTelechargement);
    };

    const handleDetailsClick = (malade) => {
        setSelectedMalade(malade);
        setShowPopup(true);
    };

    useEffect(() => {
        fetchDonations();
        fetchFacture();
    }, []);

    return (
        <div className="categories-area content">
            <img src="/_next/static/media/LogoTohano_LogoTurquoiseTohanoBanniere.5533a743.png" alt="logo" className="header-logo" />
            <h5 className="categories-area title">Donner de l'espoir, Changer de vie</h5>

            <table>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Réçu de</th>
                        <th>La somme de</th>
                        <th>Pour</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {malades.map((malade) => (
                        <tr key={malade.idUtilisateur}>
                            <td>{malade.dateNaissance}</td>
                            <td>{malade.nomMalade}</td>
                            <td>{malade.prenomMalade}Ar</td>
                            <td>{malade.cinMalade}</td>
                            <td>
                                <button onClick={() => handleDetailsClick(malade)}>Détails</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {showPopup && (
                <div className="popup">
                    <div className="popup-content">
                        <span className="close" onClick={() => setShowPopup(false)}>&times;</span>
                        <h3>Détails du malade</h3>
                        {selectedMalade && (
                            <div>
                                <p>Date : {selectedMalade.dateNaissance}</p>
                                <p>Réçu de : {selectedMalade.nomMalade}</p>
                                <p>La somme de : {selectedMalade.prenomMalade}Ar</p>
                                <p>Pour : {selectedMalade.cinMalade}</p>
                                <button onClick={imprimerFacture}>Imprimer</button>
                            </div>
                        )}
                    </div>
                </div>
            )}

            <div id="facture">
                <h3 className="categories-area title" style={{ textAlign: 'center' }}>Reçu de paiement</h3>
                {selectedMalade && (
                    <div>
                        <p>Date : {selectedMalade.dateNaissance}</p><p>Réçu de : {selectedMalade.nomMalade}</p>
                        <p>La somme de : {selectedMalade.prenomMalade}Ar</p>
                        <p>Pour : {selectedMalade.cinMalade}</p></div>
                )}
                <button onClick={imprimerFacture}>Imprimer</button>
                <button onClick={telechargerFacture}>Télécharger</button>
            </div>
        </div>
    );
};

export default FactureComponent;