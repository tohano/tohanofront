import { useEffect, useState } from "react";
import axios from 'axios';

const FactureComponent = ({ donation }) => {
    const [facture, setFacture] = useState(null);
    const imprimerFacture = () => {
        const elementFacture = document.getElementById('facture');
        window.print();
    };

    const telechargerFacture = () => {
        const elementFacture = document.getElementById('facture');

        const blob = new Blob([elementFacture.outerHTML], { type: 'application/pdf' });

        const url = URL.createObjectURL(blob);

        const lienTelechargement = document.createElement('a');
        lienTelechargement.href = url;
        lienTelechargement.download = 'facture.pdf';

        document.body.appendChild(lienTelechargement);
        lienTelechargement.click();

        URL.revokeObjectURL(url);
        document.body.removeChild(lienTelechargement);
    };
   

    useEffect(() => {
      const fetchFacture = async () => {
        try {
          const response = await axios.get('http://localhost:3000/generate-invoice');
          const factureData = response.data;
          setFacture(factureData);
        } catch (error) {
          console.error(error);
        }
      };
  
      fetchFacture();
    }, []);
  
    

    return (
        <div className="categories-area content">
        <img src="/_next/static/media/LogoTohano_LogoTurquoiseTohanoBanniere.5533a743.png" alt="logo" className="header-logo" />            <h5 className="categories-area title">Donner de l'espoir , Changer de vie</h5>

       
        <div className="row">
            <div className="col">
            </div>
            <div className="col">
            <h3 className="categories-area title" style={{ textAlign: 'center' }}>Reçu de paiement</h3>
            </div>
            <div className="col">
            </div>
            </div>
            <p>Date : {donation.date}</p>
        <p> recu de :</p>
        <p>la somme de 20000000Ar</p>
        <p>Pour : </p>
        <p>Date : {facture.date}</p>
          <p>Reçu de : {facture.recuDe}</p>
          <p>La somme de : {facture.montant}Ar</p>
          <p>Pour : {facture.description}</p>
        
        <button className="tohano-btn" onClick={imprimerFacture}>Imprimer</button>
            <button className="tohano-btn" onClick={telechargerFacture}>Télécharger</button>
            </div>
    
    );
};

export default FactureComponent;