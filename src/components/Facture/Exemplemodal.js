import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { FaEye } from 'react-icons/fa';

function Examplemodal() {
  const [show, setShow] = useState(false);
  const [malades, setMalades] = useState([]);
  const [selectedMalade, setSelectedMalade] = useState(null);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const fetchMalades = async () => {
    try {
      const response = await fetch('http://localhost:8080/listpatient');
      const maladesData = await response.json();
      setMalades(maladesData);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchMalades();
  }, []);

  const imprimerModal = () => {
    const popupWin = window.open('', '_blank', 'width=600,height=600');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
      <head>
          <title>Modal</title>
          <style>
              @media print {
                  body * {
                      visibility: hidden;
                  }
                  #modal-content,
                  #modal-content * {
                      visibility: visible;
                  }
                  #modal-content {
                      position: fixed;
                      top: 0;
                      left: 0;
                      right: 0;
                      padding: 20px;
                  }
              }
          </style>
      </head>
      <body>
          <div id="modal-content">
              ${selectedMalade && (
                <>
                  <h2>Détails des donations</h2>
                  <p>Nom du malade : ${selectedMalade.nomMalade}</p>
                  <p>Description de la demande d'aide : ${selectedMalade.demandeAides[0].descriptionDemandeAide}</p>
                  <p>Montant de l'aide financière : ${selectedMalade.demandeAides[0].donationFinancierLists[0].montantAideFinancier}</p>
                  <p>Raison de la donation financière : ${selectedMalade.demandeAides[0].donationFinancierLists[0].raisonDonationFinancier}</p>
                  <p>Date de la donation : ${selectedMalade.demandeAides[0].donationFinancierLists[0].dateDonation}</p>
                </>
              )}
          </div>
      </body>
      </html>
    `);
    popupWin.document.close();
    popupWin.print();
    popupWin.close();
  };

  const handleDetails = (malade) => {
    setSelectedMalade(malade);
  };

  return (
    <>
      <ul>
        {malades.map((malade) => (
          <li key={malade.idUtilisateur}>
            {malade.nomMalade} <FaEye onClick={() => handleDetails(malade)} />
          </li>
        ))}
      </ul>

      
      {selectedMalade && (
        <Modal show={true} onHide={() => setSelectedMalade(null)}>
          <Modal.Header closeButton>
            <Modal.Title>Détails de la demande d'aide</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Nom du malade : {selectedMalade.nomMalade}</p>
            <p>Description de la demande d'aide : {selectedMalade.demandeAides[0].descriptionDemandeAide}</p>
            <p>Montant de l'aide financière : {selectedMalade.demandeAides[0].donationFinancierLists[0].montantAideFinancier}</p>
            <p>Raison de la donation financière : {selectedMalade.demandeAides[0].donationFinancierLists[0].raisonDonationFinancier}</p>
            <p>Date de la donation : {selectedMalade.demandeAides[0].donationFinancierLists[0].dateDonation}</p>
          </Modal.Body>
          <Modal.Footer>
          <Button onClick={() => {
            if (selectedMalade) {
              imprimerModal();
              setSelectedMalade(malade);
            }
          }}>Imprimer</Button>
            <Button onClick={() => setSelectedMalade(null)}>Fermer</Button>
          </Modal.Footer>
        </Modal>
      )}
    </>
  );
}

export default Examplemodal;