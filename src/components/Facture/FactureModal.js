import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';

function Example() {
    const [show, setShow] = useState(false);
    const [malades, setMalades] = useState([]);
    const [selectedMalade, setSelectedMalade] = useState(null);
    const [facture, setFacture] = useState(null);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const fetchDonations = async () => {
        try {
            // Récupérer l'ID de l'utilisateur connecté depuis votre système d'authentification
            const userId = "1"; // Remplacez par la méthode appropriée pour récupérer l'ID de l'utilisateur

            const response = await axios.get(`http://localhost:8080/listpatient?userId=${userId}`);
            const maladesData = response.data;
            setMalades(maladesData);
        } catch (error) {
            console.error(error);
        }
    };

    const fetchFacture = async () => {
        try {
            const response = await axios.get('http://localhost:3000/generate-invoice');
            const factureData = response.data;
            setFacture(factureData);
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchDonations();
        fetchFacture();
    }, []);

    const imprimerFacture = () => {
        window.print();
    };

    return (
        <>
            <div className="categories-area content">
                <img src="/_next/static/media/LogoTohano_LogoTurquoiseTohanoBanniere.5533a743.png" alt="logo" className="header-logo" />
                <h5 className="categories-area title">Donner de l'espoir, Changer de vie</h5>

                <table>
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Réçu de</th>
                            <th>La somme de</th>
                            <th>Pour</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {malades.map((malade) => (
                            <tr key={malade.idUtilisateur}>
                                <td>{malade.dateNaissance}</td>
                                <td>{malade.nomMalade}</td>
                                <td>{malade.prenomMalade}Ar</td>
                                <td>{malade.cinMalade}</td>
                                <td>
                                    <Button className='tohano-btn' onClick={() => { handleShow(); setSelectedMalade(malade); }}>
                                        Imprimer facture
                                    </Button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Facture</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h3>{selectedMalade && selectedMalade.nomMalade}</h3>
                    <p>Date : {selectedMalade && selectedMalade.dateNaissance}</p>
                    <p>La somme de : {selectedMalade && selectedMalade.prenomMalade}Ar</p>
                    <p>Pour : {selectedMalade && selectedMalade.cinMalade}</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button className='tohano-btn' onClick={handleClose}>
                        Fermer
                    </Button>
                    <Button className='tohano-btn' onClick={imprimerFacture}>
                        Imprimer
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default Example;