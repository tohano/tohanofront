import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { FaEye } from 'react-icons/fa';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import axios from 'axios';
import { Accordion, Image } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHandHoldingMedical } from '@fortawesome/free-solid-svg-icons';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from "react-toastify";

function MyVerticallyCenteredModal(props) {
  const { selectedMalade, mediaList, ...modalProps } = props;
  const notify = () => {
    toast.success("validation de la demande d'aide reuissi", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  };
  const validerDemande = async () => {
    try {
      await axios.put(`http://localhost:8080/demandeaide/updateStatut/${selectedMalade.demandeAides[0].idDemandeAide}`);

      console.log(selectedMalade.demandeAides[0].idDemandeAide);

      //MyVerticallyCenteredModal();
      //toast.success("La demande d'aide a été validée avec succès et sera publiée !");
      notify();
      // Actualiser la page
      window.location.reload();
    } catch (error) {
      console.error("Erreur lors de la validation de la demande", error);
    }
  }

  return (
    <Modal
      {...modalProps}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {/*<h2> Demande pour un {selectedMalade.demandeAides[0].titreDemandeAide}</h2> */}

        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {selectedMalade && (
          <>
            <Accordion defaultActiveKey="0">
              <Accordion.Item eventKey="0">
                <Accordion.Header ><b>Demande faite par </b>{selectedMalade.nomMalade} </Accordion.Header>
                <Accordion.Body>
                  <ListGroup as="ul">
                    <ListGroup.Item as="li"><b>Publié le : </b>{selectedMalade.demandeAides[0].dateDemandeAide}</ListGroup.Item>
                    <ListGroup.Item as="li"><b>Date limite : </b>{selectedMalade.demandeAides[0].dateLimite}</ListGroup.Item>
                    <ListGroup.Item as="li"><b>Description : </b>{selectedMalade.demandeAides[0].descriptionDemandeAide}</ListGroup.Item>
                    <ListGroup.Item as="li" >
                      <b>Statut de la demande : </b>{selectedMalade.demandeAides[0].statut === true ? 'validé' : 'non valide'}
                    </ListGroup.Item>
                  </ListGroup>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="1">
                <Accordion.Header>Besoins financiers</Accordion.Header>
                <Accordion.Body>
                  <ListGroup as="ul">
                    <ListGroup.Item as="li">
                      montant demande : {selectedMalade.demandeAides[0].besoinFinancier.montantDemandee} Ariary
                    </ListGroup.Item>
                  </ListGroup>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="2">
                <Accordion.Header>Besoins matériels</Accordion.Header>
                <Accordion.Body>
                  <ListGroup as="ul">
                    {selectedMalade.demandeAides[0].besoinMaterielLists.map((besoinMateriel) => (
                      <ListGroup.Item as="li" key={besoinMateriel.idBesoinMateriel}>
                        {besoinMateriel.intituleBesoinMateriels} : {besoinMateriel.quantiteBesoinMateriel} 
                      </ListGroup.Item>
                    ))
                    }
                  </ListGroup>
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </>
        )}
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="colored"
        />

      </Modal.Body>
      <Modal.Footer>
        <Button className="main-btn" onClick={validerDemande} >valider</Button>
        <Button className="main-btn" onClick={props.onHide}>Fermer</Button>
      </Modal.Footer>

    </Modal>
  );
}

const ListeDemandeAides = () => {
  const [modalShow, setModalShow] = useState(false);
  const [malades, setMalades] = useState([]);
  const [selectedMalade, setSelectedMalade] = useState(null);
  const [mediaList, setMediaList] = useState([]);

  const handleDetails = (malade) => {
    setSelectedMalade(malade);
    setModalShow(true);
    fetchMediaList(malade.demandeAides[0].idDemandeAide);
  };

  const fetchMediaList = async (idDemande) => {
    try {
      const response = await axios.get(`http://localhost:8080/getmedialistbyiddemandeaide/${idDemande}`);
      setMediaList(response.data);
    } catch (error) {
      console.error('Erreur lors de la récupération des médias :', error);
    }
  };

  useEffect(() => {
    axios
      .get('http://localhost:8080/listpatient')
      .then((response) => {
        setMalades(response.data);
      })
      .catch((error) => {
        console.error('Erreur lors de la récupération des données des malades:', error);
      });
  }, []);


  return (
    <div className="next-big-thing-area pt-150">
      <div className="next-big-thing-content">
        <div>
          <h3 className="title mt-150">Liste des demandes à valider</h3>
        </div>
        <div className="row">
          {malades.map((malade) => (
            <div className="col-md-4" key={malade.idUtilisateur}>
              <Card>

                <Card.Body>
                  <FontAwesomeIcon icon={faHandHoldingMedical} style={{ color: '#3DB9CF', fontSize: '20px' }} />{" "}{malade.demandeAides[0].titreDemandeAide}
                </Card.Body>


                <ListGroup>

                  <Image

                    src={`http://localhost:8080/download/${malade.demandeAides[0].mediaLists[0]?.idMedia}?? `}
                    alt=""
                  />

                  <ListGroup.Item>{malade.nomMalade}</ListGroup.Item>
                  <ListGroup.Item>{malade.demandeAides[0].titreDemandeAide}</ListGroup.Item>
                  <ListGroup.Item style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>
                    {malade.demandeAides[0].descriptionDemandeAide}
                  </ListGroup.Item>

                </ListGroup>
                <Card.Body>
                  <Card.Link href="#">
                    <FaEye style={{ color: '#F5AC73', fontSize: '20px' }} variant="primary" onClick={() => handleDetails(malade)} />
                  </Card.Link>
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>
      </div>
      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        selectedMalade={selectedMalade}
        mediaList={mediaList}
      />
    </div>
  );
};

export default ListeDemandeAides;