import React, { useState , useEffect} from "react";
import { Modal,Col,Image ,Button} from "react-bootstrap";
import ModalMalade from "./ModalMalade";

const ListeDemande = () => {
  const [malades, setMalades] = useState([]);
  const [selectedMalade, setSelectedMalade] = useState(null);

  useEffect(() => {
    fetch("http://localhost:8080/listpatient")
      .then((response) => response.json())
      .then((data) => setMalades(data))
      .catch((error) => {
        console.error("Une erreur s'est produite lors de la récupération des données :", error);
      });
  }, []);

  return (
    <div>
    {malades.map((malade, index) => (
      <Col lg={4} md={6} key={malade.idUtilisateur}>
      
        <div className={`pt-25 pb-25`}>
            <div className="justify-content-center">
              <Image
                src={malade.photo}
                alt={malade.nom}
                className="rounded-circle mx-auto d-block"
              />
            </div>
            <div>
              <div className="news-content" style={{ width: 374, height: 400 }}>
                <div className="text">
                  <span>{malade.pseudo}</span>
                  <span style={{ marginLeft: "10px" }}></span>
                  <div key={`content-${malade.idUtilisateur}`}>
                    <h3 key={`title-${malade.idUtilisateur}`}>{malade.demandeAides[0].titreDemandeAide}</h3>
                    {malade.demandeAides[0].statut === true && (
                      <div className="flaticon-check" key={`check-${malade.idUtilisateur}`}>
                        <p>{malade.demandeAides[0].status}</p>
                      </div>
                    )}
                  </div>
                </div>
                <div style={{ color: "#202020", marginTop: "5px" }}>
                  <div className="row">
                    <div className="col">publié le :</div>
                    <div className="col">{malade.demandeAides[0].dateDemandeAide}</div>
                  </div>
                  {/*malade.demandeAides[0].besoinMaterielLists.map((besoinMateriel) => (
                    <div className="row" key={besoinMateriel.idBesoinMateriel}>
                      <div className="col">Quantité du besoin :</div>
                      <div className="col">{besoinMateriel.quantiteBesoinMateriel}</div>
                    </div>
                  ))*/}
                  <div className="row">
                    <div className="col">pour :</div>
                    <div className="col">{malade.nomMalade}</div>
                  </div>
                  <div className="row">
                    <div className="col">date limite</div>
                    <div className="col">{malade.demandeAides[0].dateLimite}</div>
                  </div>
                  <div className="row">
                    <div className="col">Type de demande :</div>
                    <div className="col">{malade.demandeAides[0].titreDemandeAide}</div>
                  </div>
                  {malade.demandeAides[0].besoinFinancierLists.map((besoinFinancier) => (
                    <div className="row" key={besoinFinancier.idBesoinFinancier}>
                      <div className="col">Montant demandé:</div>
                      <div className="col">{besoinFinancier.montantDemandee}</div>
                    </div>
                  ))}
                  <div className="row">
                    <div className="col">Description:</div>
                    <div className="col publication">{malade.demandeAides[0].besoinMaterielLists[0].intituleBesoinMateriels}</div>
                  </div>
                  {malade.demandeAides[0].besoinMaterielLists.map((besoinMateriel) => (
                    <div className="row" key={besoinMateriel.idBesoinMateriel}>
                      <div className="col">Quantité:</div>
                      <div className="col">{besoinMateriel.quantiteBesoinMateriel}</div>
                    </div>
                  ))}
                </div>
                <div className="d-flex justify-content-center">
                  
                  <Button className="main-btn" onClick={() => setSelectedMalade(malade)} > Voir plus</Button>
              
                  
                </div>
              </div>
            </div>
          </div>
        </Col>
        ))}
        <ModalMalade selectedMalade={selectedMalade} />
      </div>
    
  );
};

export default ListeDemande;