import axios from "axios";
import { useState, useEffect } from "react";
import { Button, Col, Container, Modal, Row } from "react-bootstrap";

const UnDemande = ({dataMalade, data}) => {
  const [show, setShow] = useState(false);
  return (
    <div
      className="news-content"
      style={{ width: 370, height: 500, marginBottom: 50, backgroundColor:"#f7f7f9", padding:20 }}
    >
      <div className="text">
        <span>Pseudo</span>
        <span style={{ marginLeft: "10px" }}></span>
        <div>
          <h3>{data.titreDemandeAide}</h3>

          <div className="flaticon-check">
            <p>status</p>
          </div>
        </div>
      </div>
      <div className="d-flex justify-content-center">
        <Button
          className="main-btn"
          onClick={() => {
            setShow(true);
          }}
        >
          Voir plus
        </Button>
      </div>
      <UnDemandeDetailsModal
        show={show}
        onHide={() => {
          setShow(false);
        }}
      />
    </div>
  );
};

const UnDemandeDetailsModal = ({ show, onHide }) => {
  return (
    <Modal
      show={show}
      size="lg"
      dialogClassName="modal-1200-600"
      contentClassName="modal-content-1200-600"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">Titre</Modal.Title>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={onHide}
        ></button>
      </Modal.Header>
      <Modal.Body>
        <Container>Bonjour modal</Container>
      </Modal.Body>
      <Modal.Footer>
        <button className="main-btn" onClick={onHide}>
          Annuler
        </button>
        <button type="submit" className="main-btn">
          Valider
        </button>
      </Modal.Footer>
    </Modal>
  );
};

const DemandeAvalider = ({dataMalade}) => {

  let demandes = []

  let data = dataMalade.map((utilisateur) => utilisateur.demandeAides);
  console.log("Liste demande Aide : ",data);
  console.log("demandes===============",demandes)

  return (
    <section className="mt-150">
      <Container>
        <Row>
          {/* BLOCK TITRE */}
          <div>Listes des demande a valider</div>
        </Row>
        <Row>
          {
            data.map((demande)=>{
              console.log("Data Un Malade : ", demande);
              return(
                <UnDemande dataMalade={dataMalade} data={demande} />
                );
              })
            }
        </Row>
      </Container>
    </section>
  );
};

export default DemandeAvalider;
