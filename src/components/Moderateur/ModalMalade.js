import React from "react";
import { Modal, Container, Row, Col } from "react-bootstrap";

const ModalMalade =({ selectedMalade }) => {

  return (
    <Modal.Body>{selectedMalade && selectedMalade.nomMalade}</Modal.Body>
      );
    };
    
    export default ModalMalade;
