import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Title from "../Reuseable/Title";
import axios from 'axios';
import { useRouter } from 'next/router';

export default function Step1() {

  const router = useRouter();
  let user = null;

  if (typeof sessionStorage !== 'undefined') {
    user = JSON.parse(sessionStorage.getItem('connectedUser'));
  }
  
  if(user != null){
const [formData, setFormData] = useState({
    statut:false , 
    malade:{idUtilisateur : user.id}
  });

  const [places, setPlaces] = useState([]);
  const { titreDemandeAide, descriptionDemandeAide, lieu, latitude, longitude, dateLimite } = formData;

  const handleChange = (e) => {
    const { name, value } = e.target;

     if (name === 'lieu') {
      setFormData({ ...formData, [name]: value });
      fetchPlaces(value);
    } else {
      setFormData({ ...formData, [name]: value });
    }
  };

  const fetchPlaces = (search) => {
    fetch(
      `https://api.geoapify.com/v1/geocode/autocomplete?text=${search}&format=json&apiKey=41695e5025314eb79e5f97eb03d067e3`
    )
      .then((response) => response.json())
      .then((result) => {
        setPlaces(result.results);
        console.log(result.results)
      })
      .catch((error) => console.log('error', error));
  };

  const handlePlaceClick = (place) => {
    setFormData({
      ...formData,
      lieu: place.formatted,
      longitude: place.bbox.lon1,
      latitude: place.bbox.lat1 
    });
    setPlaces(place)
    console.log(lieu)
    console.log(place.bbox.lon1,place.bbox.lat1)
  };

  function validateStep1() {
    return titreDemandeAide && descriptionDemandeAide && lieu && dateLimite;
  }

  const resetForm = (e) => {
    e.preventDefault();
    setFormData({
      titreDemandeAide: "",
      descriptionDemandeAide: "",
      lieu: "",
      places:""
    });
  };

  const handleValider = async (e) => {
    e.preventDefault();
    //handleNext();
    console.log(JSON.stringify(formData))
    await axios.post(`http://localhost:8080/patient/addInfoRequest/${user.id}`,formData)
    .then(function (response) {
        console.log("Post demande aide:" + response.data)
        sessionStorage.setItem('idDemandeAide',response.data)
        
        // MILA MANAMBOATRA ALERT HO AN ILAY ZAVATRA VALIDE
    }).catch(function (error){
       console.error("Misy diso ooo" + error)
     });

    router.push("/patient/addRequestStep2")
  }

  return (
    
    <section className="contact-form-area mt-150">
        <Container>
          <form onSubmit={handleValider} >
          <Row className="justify-content-center" >
            <Col lg={8}>
              <Title title="Informations de votre demande" tagline="Etape 1/3" className="text-center" />
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col lg={8}>
              <div className="conact-form-item">
                <Row>
                  <Row>
                    <Col lg={6} md={6}>
                      <div className="input-box mt-20 m-20">
                        <h5 className='text-orange'>Objet de votre demande</h5>
                        <input
                          type="text"
                          name="titreDemandeAide"
                          placeholder="Sujet de votre demande"
                          value={titreDemandeAide}
                          onChange={handleChange}
                        />
                      </div>
                    </Col>
                    <Col lg={6} md={5} >
                      <div className="input-box mt-20 m-20">
                        <h5 className='text-orange'>Vous en avez besoin quand?</h5>
                        <input
                          type="date"
                          name="dateLimite"
                          //placeholder="Sujet de votre demande"
                          value={dateLimite}
                          onChange={handleChange}
                          
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row >
                    <div className={`input-box mt-20`}>
                      <h5 className='text-orange'>Donner nous une description de la situation actuelle</h5>
                      <textarea
                        name="descriptionDemandeAide"
                        placeholder="Racontez nous cela"
                        value={descriptionDemandeAide}
                        onChange={handleChange}
                        style={{ minHeight: '200px', maxWidth:'750px' }}
                      />
                    </div>
                  </Row>
                  <Row>
                    <div className={`input-box mt-20`}>
                      <h5 className='text-orange'>Indiquer au donateur votre adresse</h5>
                      <input
                        type="text"
                        name="lieu"
                        placeholder="Où les donateurs pourront vous rencontrer?"
                        value={lieu}
                        onChange={handleChange}
                      />
                    </div>
                    <ul className='ml-5'>
                      {places && Array.isArray(places) ? (places.map((place) => (
                          place.country === "Madagascar" && (
                            <li
                              key={place.id} 
                              onClick={() => handlePlaceClick(place)}
                              className='suggestion-li'
                            >
                              {place.formatted}
                            </li>
                          )
                        ))) : null}
                    </ul>
                  </Row>
                  <Col lg={12}>
                    <div className="input-box mt-20 text-center">
                      <button className="main-btn main-btn-2 mr-150" onClick={resetForm}>
                        Réinitialiser
                      </button>
                      {validateStep1() ? (
                        <button className="main-btn" type='submit'>
                          Valider
                        </button>
                      ) : (
                        <button className="main-btn" disabled>
                          Valider
                        </button>
                      )}
                    </div>
                  </Col>
                </Row>
              </div>
              <p className="form-message"></p>
            </Col>
          </Row>
          </form>
        </Container>
      </section>
    
  );
  }else {
    return console.log("user null : " + user)
  }
  
}