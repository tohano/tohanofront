"use client";


import { useState } from "react";
import axios from 'axios';
import { useRouter } from 'next/router';
import AddRequestStep1 from "./addRequestStep1";
import AddRequestStep2 from "../../pages/patient/addRequestStep2";
import AddRequestStep3 from "../../pages/patient/addRequestStep3";

export default function Formulaire() {
  const router = useRouter();
  const [currentStep, setCurrentStep] = useState(1);
  
  // const idUserConnected = window.sessionStorage.getItem('connectedUser').id;
  // alert(idUserConnected)
  

  const handleNext = () => {
    alert(JSON.stringify(formData))
    setCurrentStep(currentStep + 1);
    console.log('Données du formulaire 1 soumises :', formData);
  };

  const handlePrevious = () => {
    setCurrentStep(currentStep - 1);
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    //MILA OVAINA NY AM ADD DEMANDE
    await axios.post("http://localhost:8080/inscritpion/malade",formData)
    .then(function (response) {
        console.log("Post io" + response.data)
        // Stockage des données dans sessionStorage à tester
        if (typeof window !== 'undefined') {
      // Vérification de la disponibilité de sessionStorage
      sessionStorage.setItem('connectedUser', JSON.stringify(formData));
    }
        

    }).catch(function (error){
        console.error("Misy diso ooo" + error)
        
    });
    console.log('Données du formulaire 1 + 2soumises :', formData);
    
    router.push("/maladeHome")
  };

  const renderStep = () => {
    switch (currentStep) {
      case 1:
        return (
          <AddRequestStep1
          />
          
        );
      case 2:
        return (
          <AddRequestStep2
          />
        );
      case 3:
        return (
          <AddRequestStep3
          />
        );
      default:
        return null;
    }
  };

  return (renderStep());
}