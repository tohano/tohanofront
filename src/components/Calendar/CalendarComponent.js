import FullCalendar from '@fullcalendar/react' // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid' // a plugin!
import interactionPlugin from "@fullcalendar/interaction"

export default function CalendarComponent(props){
    const handleDateClick = (arg)=>{
        alert(arg.event.title);
    }
    return (
        <div style={{marginTop:150, marginBottom:50, objectFit:"cover"}}>
            <FullCalendar
                plugins={[ dayGridPlugin, interactionPlugin ]}
                initialView="dayGridMonth"
                weekends={false}
                events={[
                    { title: 'event 1', date: '2024-01-01' },
                    { title: 'event 2', date: '2024-01-18' }
                ]}
                height={"90vh"}
                eventClick={handleDateClick}
                />
        </div>
    );
}
