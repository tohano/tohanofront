import React, { useState, useEffect } from 'react';
import bg1 from "@/images/tohanobanner1.png";
import bg2 from "@/images/tohanobanner2.png";
import bg3 from "@/images/tohanobanner3.png";
import Image from 'next/image';
import { Margin } from '@mui/icons-material';

export const Titlehowtowork = () => {
  const data = [
    {
      id: 1,
      bg: bg1,
      text: "",
      title: "",
    },
    {
      id: 2,
      bg: bg2,
      text: "s",
      title: "",
    },
    {
      id: 3,
      bg: bg3,
      text: "",
      title: "",
    },
  ];

  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    const timer = setInterval(() => {
      setCurrentIndex(currentIndex => (currentIndex + 1) % data.length);
    }, 5000);

    return () => {
      clearInterval(timer);
    };
  }, []);

  const currentItem = data[currentIndex];

  return (
    <section className="page-title-area bg_cover"  style={{ marginTop: '200px' }}>
      <div className="background-image" style={{ position: 'relative', width: '1896px', height: '400px',marginLeft:'20px' }}>
        <Image
          src={currentItem.bg}
          alt=""
          layout="fill"
          objectFit="cover"
        />
        <div className="content about-title">
          <h2>{currentItem.title}</h2>
          <p>{currentItem.text}</p>
        </div>
      </div>
    </section>
  );
};