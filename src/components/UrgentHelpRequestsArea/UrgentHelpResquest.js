

import React  from "react";
import { Image } from "react-bootstrap";
import Link from "../Reuseable/Link";

const UrgentHelpResquest = ({urgentHelpResquest = {}}) => {
    console.log(urgentHelpResquest.image.src);
    const {id,image, title}= urgentHelpResquest;
    return (
        <div className={"explore-projects-item ml-30 pb-50"}>
        <div className="explore-projects-thumb">
            <Image src={image.src} alt="urgent-request" />
            <a href="#">
            <i className="fa fa-heart"></i>
            </a>
        </div>

        <div className="explore-projects-content">
            <Link href="/#">
                <h3 className="title">{title}</h3>
            </Link>
        </div>
        </div>
    );
};

export default UrgentHelpResquest;
