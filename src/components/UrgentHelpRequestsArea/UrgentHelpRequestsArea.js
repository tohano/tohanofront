import { urgentHelpRequestsArea } from "@/data/urgentHelpRequestsArea";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Title from "../Reuseable/Title";
import UrgentHelpResquest from "./UrgentHelpResquest";

const { tagline, title, urgentHelpRequests } = urgentHelpRequestsArea;


const UrgentHelpRequestsArea = ({ className = "pt-25 pb-25" }) => {
    return (
        <section className={`explore-projects-area bg-orange-clair ${className}`}>
        <Container>
            <Row className="justify-content-center">
                <Col lg={8}>
                    <Title tagline={tagline} title={title} className="text-center" />
                </Col>
            </Row>
            <div className="explore-project-active ">
                    <div className="swiper-wrapper justify-content-center">
                        {urgentHelpRequests.map((urgentHelpRequest) => (
                            // console.log(urgentHelpRequest.image.src),
                            <div key={urgentHelpRequest.id}>
                                <UrgentHelpResquest urgentHelpResquest ={urgentHelpRequest} />
                            </div>
                            
                        ))}
                    </div>
            </div>
      </Container>
    </section>
  );
}

export default UrgentHelpRequestsArea;