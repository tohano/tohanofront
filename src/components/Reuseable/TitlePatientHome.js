
import React from "react";
import baniere from "@/images/malade/banniereHomePatient.jpg"
import { Col, Container, Row } from "react-bootstrap";

const PageTitle = ({ title = "", page = "", parent = "" }) => {
  return (
    <section
      className="page-title-area bg_cover"
      style={{ backgroundImage: `url(${baniere.src})` }}
    >
      <Container>
        <Row>
          <Col lg={12}>
            <div className="page-title-content">
              <h3 className="title">{title}</h3>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default PageTitle;
