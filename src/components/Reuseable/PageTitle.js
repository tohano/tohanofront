
import postoperatoire1894x329 from "@/images/malade/postoperatoire1894x329.jpg";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const PageTitle = ({ title = "", page = "", parent = "" }) => {
  return (
    <section
      className="page-title-area bg_cover"
      style={{ backgroundImage: `url(${postoperatoire1894x329.src})` }}
    >
      <Container>
        <Row>
          <Col lg={12}>
            <div className="page-title-content">
              <h3 className="title">{title}</h3>
              
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default PageTitle;
