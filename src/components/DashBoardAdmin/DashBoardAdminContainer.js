import TopBox from "../Charts/TopBox";
import PieChartBox from "../Charts/PieChartBox";
import BigChartBox from "../Charts/BigChartBox";
import BarChartBox from "../Charts/BarChartBox";
import ChartBox from "../Charts/ChartBox";
import {barChartBoxVisit} from "@/data/data";
import {barChartBoxRevenue} from "@/data/data";
import {chartBoxUser} from "@/data/data";
import {chartBoxProduct} from "@/data/data";
import {chartBoxRevenue} from "@/data/data";
import {chartBoxConversion} from "@/data/data";
const DashBoardAdmin = () => {//<ChartBox {...boxUser}/>
  
  
    const visit = barChartBoxVisit;
    const revenue = barChartBoxRevenue;
    const boxUser = chartBoxUser;
    const boxProduct = chartBoxProduct;
    const boxRevenue = chartBoxRevenue;
    const boxConversion = chartBoxConversion;
  return (
    <div className="container-dashboard">
      <div className="box-dashboard box1"><TopBox/></div>
      <div className="box-dashboard box2"><ChartBox {...boxUser}/></div>
      <div className="box-dashboard box3"><ChartBox {...boxProduct}/></div>
      <div className="box-dashboard box4"><PieChartBox/></div>
      <div className="box-dashboard box5"><ChartBox {...boxRevenue}/></div>
      <div className="box-dashboard box6"><ChartBox {...boxConversion}/></div>
      <div className="box-dashboard box7"><BigChartBox/></div>
      <div className="box-dashboard box8"><BarChartBox {...visit}/></div>
      <div className="box-dashboard box9"><BarChartBox {...revenue}/></div>
    </div>
  );
};

export default DashBoardAdmin;
