import postoperatoire770x438 from "@/images/malade/postoperation770x438.jpg";
import { besoins , projectDetailsStory } from "@/data/projectsArea";
import React, { useEffect, useRef, useState } from "react";
import { Col, Image, Row } from "react-bootstrap";
import { useRouter } from "next/router";
import axios from "axios";

const { id, text, lists, text2, items, text3, image, images } =  projectDetailsStory;

const ProjectDetailsStory = ({ getClassName, idDemande,  descriptionDemandeAide}) => {

  // console.log("Dans project Details Story => " + descriptionDemandeAide);
  // console.log("id passé dans project details story ::::::::::::::::: " + idDemande + " " + typeof(idDemande));

  const fetchData = async () => {
    try {
        const url= "http://localhost:8080/demandeaide/list";
        const response = await axios.get(url);
        // console.log("Data Malade Axios :: ", response.data);
        // setDataMalade(response.data);
        response.data.map(res=> {
          if(res.idDemandeAide == idDemande){
            const test = res;
            // console.log("res project detail story" + typeof(test) + " " + test);
          }
      })
    } catch (error) {
      // console.log("Erreur : ", error);
    }
  };

  useEffect(() => {
    fetchData();
    // console.log("useEffect dans project detail Story en action ")
  },[idDemande]);


  return (
    <div className={getClassName?.(id)} id={id} role="tabpanel">
      <div className="project-details-content-top">
        {/* <p>{text}</p>
        <ul>
          {lists.map((list, i) => (
            <li key={i}>
              <i className="flaticon-check"></i> {list}
            </li>
          ))}
        </ul> */}
        {/* <div className="project-details-thumb">
          <Image src={postoperatoire770x438.src} alt="" />
        </div> */}
      </div>
      <div className="project-details-item">
        <p>{descriptionDemandeAide}</p>
        {/* {besoins.map((besoin , className="mt-25") => (
          <div className={`item ${className}`} key={besoin.id}>
            <i className={(besoin.type==true)?"flaticon-checkmark":"flaticon-minus"}></i>
            <h5 className="title">{besoin.type}</h5>
            <p>{(besoin.type=="materiel")?(besoin.quantite + " " + besoin.intitule):(besoin.montant + " Ar")}</p>
          </div>
        ))} */}
        {/* <Row>
          {images.map((image, i) => (
            <Col lg={6} md={6} sm={6} key={i}>
              <div className="project-details-thumb">
                <Image src={image.src} alt="" />
              </div>
            </Col>
          ))}
        </Row>
        <p className="text">{text3}</p> */}
      </div>
    </div>
  );
};

export default ProjectDetailsStory;
