import { helpRequestsTbns, projectDetailsStory, projectDetailsTabBtns } from "@/data/projectsArea";
import React, { useEffect, useRef, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import ProjectDetailsComments from "./ProjectDetailsComments";
import ProjectDetailsFaq from "./ProjectDetailsFaq";
import ProjectDetailsSidebar from "./ProjectDetailsSidebar";
import ProjectDetailsStory from "./ProjectDetailsStory";
import ProjetFactureArea from './ProjetFactureArea';
import ProjectDetailsUpdates from "./ProjectDetailsUpdates";
import DemandeAideBesoins from "./DemandeAideBesoins";
import axios from "axios";
import { malade_liste } from "@/data/data_malade";
import { useRouter } from "next/router";

// const demandeAides = malade_liste.map((utilisateur)=>utilisateur.demandeAides);

const ProjectDetailsContent = ({idDemande}) => {
  
  const [current, setCurrent] = useState("description");

  // console.log("hita anaty project Details Content =====================>" + idDemande);
  
  console.log("project details content  idDemande = " + idDemande);
  const [demande, setDemande] = useState({});
  const demandeAide = [];
  const listeMedia = [];
  const fetchData = async () => {
    try {
        const url= "http://localhost:8080/demandeaide/list";
        const response = await axios.get(url);
        // console.log("Data Malade Axios :: ", response.data);
        // setDataMalade(response.data);
        response.data.map(res=> {
          if(res.idDemandeAide == idDemande){
            setDemande(res);
          }
      })
    } catch (error) {
      // console.log("Erreur : ", error);
    }
  };

  useEffect(() => {
    fetchData();
    // console.log("demandeAide Content  ====> ",demandeAide);
    // console.log(demande);
  }, [idDemande]);

  

  const getClassName = (id = "") => {
    const active = current === id;
    return `tab-pane animated${active ? " fadeIn show active" : ""}`;
  };

  return (
    <section className="project-details-content-area pb-110">
      <Container>
        <Row className="justify-content-center">
          <Col lg={8}>
            <div className="tab-btns">
              <ul className="nav nav-pills" id="pills-tab" role="tablist">
                {helpRequestsTbns.map(({ id, name }) => (
                  <li key={id} className="nav-item" role="presentation">
                    <a
                      onClick={() => setCurrent(id)}
                      className={`nav-link cursor-pointer${
                        id === current ? " active" : ""
                      }`}
                      role="tab"
                    >
                      {name}
                    </a>
                  </li>
                ))}
              </ul>
            </div>
            <div className="tab-content" id="pills-tabContent">
              <ProjectDetailsStory getClassName={getClassName} idDemande={idDemande} descriptionDemandeAide={demande.descriptionDemandeAide} />
              <DemandeAideBesoins getClassName={getClassName} idDemande={idDemande}/>
              <ProjetFactureArea getClassName={getClassName} idDemande={idDemande} />
              {/* <ProjectDetailsUpdates getClassName={getClassName} />
              <ProjectDetailsComments getClassName={getClassName} /> */}
            </div>
          </Col>
          <Col lg={4} md={7} sm={9}>
            <ProjectDetailsSidebar idDemande={idDemande} />
          </Col>
        </Row>
      </Container>
    </section>
  );
}

export default ProjectDetailsContent;
