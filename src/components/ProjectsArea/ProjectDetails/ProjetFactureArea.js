import React, { useEffect, useState } from "react";
import jsPDF from 'jspdf';

const ProjetFactureArea = ({ getClassName, idDemande}) => {
  const [donation,setDonation] = useState([]);
  const [donationMat,setDonationMat] = useState([]);


  const fetchData = async () => {
    try {
      const user = JSON.parse(sessionStorage.getItem('connectedUser'));
      const response = await fetch(`http://localhost:8080/getDonationByDonateur/${idDemande}/${user.id}`);
      const responseMat = await fetch(`http://localhost:8080/getDonationMaterielByDonateur/${idDemande}/${user.id}`);
      
      const data = await response.json();
      const dataMat = await responseMat.json();
      
      setDonation(data);
      setDonationMat(dataMat);
    } catch (error) {
    }
  };

  const generatePDF = (param) => {
    const doc = new jsPDF();
    const image = new Image();
    image.src = '/_next/static/media/LogoTohano_LogoTurquoiseTohanoBanniere.5533a743.png';

    image.onload = () => {
      doc.addImage(image, 'PNG', 150, 5, 50, 50); // Ajoute l'image au PDF
      doc.save('Donation.pdf');
    };
    doc.text('Tohano', 10, 10);
    doc.text('contact@tohano.com', 10, 20);
    doc.text('+261 34 86 068 86', 10, 30);
    doc.text('Faravohitra Antananarivo, Madagascar', 10, 40);
    doc.text('Facture de donation ', 70, 60); // Ajoutez le contenu de votre PDF ici
    doc.text(`Raison : ${param.raisonDonationFinancier}`, 20, 70);
    doc.text(`Montant : ${param.montantAideFinancier} Ariary`, 20, 80);
    doc.text(`Paiement le  : ${param.dateDonation.split("-")[2]}-${param.dateDonation.split("-")[1]}-${param.dateDonation.split("-")[0]}`, 20, 90);
    doc.text('Tohano vous remercie de votre générosité', 70, 120);
  };

  useEffect(() => {
    fetchData();
  }, [idDemande]);

  return (
    <div className={getClassName?.("participation")} id="participation" role="tabpanel">
      
      <div className="project-details-item">
        <h4 style={{marginTop: '3em'}}>Donation financiére</h4>
        { donation.length == 0 ? <span class="text-info p-3">
            Votre aide financiére sauverais un patient. 
            N'hésitez pas à participer!
          </span>:
          <div>
            <table  class="table">
              <thead>
                <th>Raison</th>
                <th>Montant</th>
                <th>Date</th>
                <th>Etat du paiement</th>
              </thead>
              <tbody>
                { donation.map(don => (
                    <tr>
                      <td>{don.raisonDonationFinancier}</td>
                      <td>{don.montantAideFinancier}</td>
                      <td>{(don.dateDonation!=undefined)?don.dateDonation.split("-")[2] +
                         "-" + don.dateDonation.split("-")[1] + 
                         "-" + don.dateDonation.split("-")[0]:''}</td>
                      <td>{don.etatPaiement}</td>
                      <td><button className="main-btn" onClick={() => generatePDF(don)}>Imprimer</button></td>
                    </tr>
                  ))
                } 
              </tbody>
            </table>
          </div>
        }
        <br /><br /><br />
        <h4>Donation matérielle</h4>
        { donationMat.length == 0 ? <span class="text-info p-3">
            Votre aide matérielle sauverais un patient. 
            N'hésitez pas à participer!
          </span>:
          <div>
            <table  class="table">
              <thead>
                <th>Raison</th>
                <th>Montant</th>
                <th>Date</th>
                <th>Etat du paiement</th>
              </thead>
              <tbody>
                { donationMat.map(don => (
                    <tr>
                      <td>{don.raisonDonationFinancier}</td>
                      <td>{don.montantAideFinancier}</td>
                      <td>{don.dateDonation}</td>
                      <td>{don.etatPaiement}</td>
                    </tr>
                  ))
                } 
              </tbody>
            </table>
          </div>
        }
        
        
        
      </div>
    </div>
  );
};

export default ProjetFactureArea;
