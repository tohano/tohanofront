
import postoperatoire670x537 from "@/images/malade/postoperatoire670x537.jpg";
import { projectDetailsArea } from "@/data/projectsArea";
import React, { useEffect, useState } from "react";
import { Carousel, Col, Container, Image, Row } from "react-bootstrap";
import axios from "axios";
import { FeedbackTwoTone } from "@mui/icons-material";


const {
  thumb,
  flag,
  tagline,
  country,
  title,
  pledged,
  backers,
  daysLeft,
  raised,
  goal,
  socials,
} = projectDetailsArea;

const ProjectDetailsArea = ({idDemande}) => {
  
  const [demandeAide,setDemandeAide] = useState({});
  let [media, setMedia] = useState(0);
  
  const fetchData = async () => {
    try {
      const response = await fetch(`http://localhost:8080/demandeaide/${idDemande}`);
      const medias= await fetch(`http://localhost:8080/getmedialistbyiddemandeaide/${idDemande}`)
      if (!response.ok || !medias.ok) {
        throw new Error(`Erreur HTTP! Statut : ${response.status}`);
      }
      const data = await response.json();
      const list = await medias.json();
        // list.map((idMedia)=>{
        // mediaList.push(idMedia);
        setMedia(list[0]);
      // });
      setDemandeAide(data);
    } catch (error) {
      // console.error("Erreur lors de la recherche de la demande d'aide :", error);
    // } finally {
    //   console.log("id demande aide encore ici ProjectDetailsArea anaty fetch finally =======================>"+ idDemande);
    }
  };
  

  // console.log("hita anaty project Details après fetch =====================>" + idDemande);
  useEffect(() => {
    fetchData();
    // console.log("media après fetch data ::::::::::::" + media)
  }, [idDemande]);

  // console.log("ivelany",demandeAide); 
  
  return (
        <section className="project-details-area pt-120 pb-190">
          <Container>
            <Row>
              <Col lg={7}>
                <div className="project-details-thumb">
                {/* // <Carousel>
                // { mediaList.map((media) =>{     
                  
                //     <Carousel.Item>
                //       <Carousel.Caption> */}
                <Image src={`http://localhost:8080/download/${media}`} alt="images demande d'aide" />
                {/* //       </Carousel.Caption>
                //     </Carousel.Item
                // })}
                // </Carousel> */}
                </div>
              </Col>
              <Col lg={5}>
                <div className="project-details-content">
                  {/* <div className="details-btn">
                    <span>{tagline}</span>
                    <div className="flag">
                      <Image src={flag.src} alt="" />
                      <p>{country}</p>
                    </div>
                  </div> */}
                  <h3 className="title">
                    {demandeAide.titreDemandeAide}
                  </h3>
                  <div className="project-details-item">
                    {/* <div className="item text-center">
                      <h5 className="title">${pledged}</h5>
                      <span>Pledged</span>
                    </div>
                    <div className="item text-center">
                      <h5 className="title">{backers}</h5>
                      <span>Backers</span>
                    </div> */}
                    <div className="item text-center">
                      <span>Date limite</span>
                      <h5 className="title">
                        {(demandeAide.dateLimite!=undefined)?demandeAide.dateLimite.split("-")[2] +
                         "-" + demandeAide.dateLimite.split("-")[1] + 
                         "-" + demandeAide.dateLimite.split("-")[0]:''}
                      </h5>
                      
                    </div>
                  </div>
                  {/* <div className="projects-range">
                    <div className="projects-range-content">
                      <ul>
                        <li>Raised:</li>
                        <li>{raised}%</li>
                      </ul>
                      <div className="range"></div>
                    </div>
                  </div>
                  <div className="projects-goal">
                    <span>
                      Goal: <span>{goal} USD</span>
                    </span>
                  </div> */}
                  <div className="project-btn mt-25">
                    <a className="main-btn" href="#besoins">
                      Faire une donation
                    </a>
                  </div>
                  {/* <div className="project-share d-flex align-items-center">
                    <span>Share this Project</span>
                    <ul>
                      {socials.map(({ id, icon, href }) => (
                        <li key={id}>
                          <a href={href}>
                            <i className={icon}></i>
                          </a>
                        </li>
                      ))}
                    </ul>
                  </div> */}
                </div>
              </Col>
            </Row>
          </Container>
        </section>
  );
 }
export default ProjectDetailsArea;
