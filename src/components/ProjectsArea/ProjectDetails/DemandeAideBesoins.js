import BesoinsFinanciers from "@/components/FaqArea/BesoinsFinanciers";
import BesoinsMateriels from "@/components/FaqArea/BesoinsMateriels";
import { projectDetailsFaq } from "@/data/projectsArea";
import React from "react";

const { faqs, id } = projectDetailsFaq;

const DemandeAideBesoins = ({ getClassName , idDemande}) => {
  // console.log("id demande aide passé en props à demande aide besoins :::::::::" + idDemande);
  return (
    <div className={getClassName(id)} id={id} role="tabpanel">
      {/* <h3>Besoins financiers</h3>
      <BesoinsFinanciers idDemandeAide={idDemandeAide} className="mt-70" />   */}
      <h3>Besoins matériels</h3>
      <BesoinsMateriels idDemande={idDemande} className="mt-70" />
    </div>
  );
};

export default DemandeAideBesoins;
