import Faqs from "@/components/FaqArea/Faqs";
import { projectDetailsFaq } from "@/data/projectsArea";
import React from "react";

const { faqs, id } = projectDetailsFaq;

const ProjectDetailsFaq = ({ getClassName , besoinMaterielLists , besoinFinancierLists}) => {
  const besoins = [ besoinMaterielLists, besoinFinancierLists];
  return (
    <div className={getClassName(id)} id={id} role="tabpanel">
      <Faqs faqs={besoins} className="mt-70" />
    </div>
  );
};

export default ProjectDetailsFaq;
