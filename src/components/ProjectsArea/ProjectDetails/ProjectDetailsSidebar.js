import { projectDetailsSidebar, needsDetails } from "@/data/projectsArea";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { Container, Image, Modal, ProgressBar } from "react-bootstrap";
import PaiementForm from "src/pages/formPaiementStripe";


//   raised = res.data;
   //console.log("retour vola ----> " + res)

const { info, perks } = projectDetailsSidebar;
const FormModal = ({ show, onHide, idDemande }) => {

  return (
    <Modal
      show={show}
      size="lg"
      dialogClassName="modal-500-500"
      contentClassName="modal-content-500-500"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">Donation financière</Modal.Title>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={onHide}
        ></button>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <div >
            <h5>Insérez le montant que vous souhaiter donner à ce malade (en Ariary)</h5>
            <h6>*minimun de 5.000 ar </h6>
            <PaiementForm idDemande={idDemande}/>
          </div>

        </Container>
      </Modal.Body>
      
    </Modal>
  );
};

const ProjectDetailsPark = ({ perk = {}, idDemande}) => {
  const { id, image, sold, off, amount, date, claimed, totalClaimed } = perk;
  const [show, setShow] = useState(false);
  //eto no apetaka ny resultat avy any anaty bdd
  // alert(idDemande)
  const [raised,setRaised] = useState(0);

  const res = async () => {
    
    const total = await fetch(`http://localhost:8080/donation/getall/${idDemande}`);
    const data = await total.json();
    setRaised(data);
    // alert("data +++++++++++++ " + data);
  
  }

  const progressDons = ()=>{
    return (raised*100)/perk.montantDemandee;
  }
  useEffect(() => {
    //ato mi calcule
    //alert("ito poux e --2---PARK---//////*------"+idDemande)
    res();
  },[])
  //alert(raised)
  // console.log(raised)
  return (
    <div
      className={`project-details-park mt-30 box${id === 2 ? " item-2" : ""}`}
      id={`besoin${perk.idBesoin}`}
    >
      <h4 className="title">Besoin financier</h4> 

      <span>{(perk.montantDemandee + " Ar")}</span>
      <p>

      </p> 
      { <ul>
        {raised > 0 ? (<li>Récolté : {raised} ar</li>) : (<li>0%</li>)  }
        <li>

          <ProgressBar variant="info" now={progressDons()} label={`${progressDons().toFixed(2)}%`} style={{backgroundColor:"#e9e9ee", color:"black"}}/>
        </li>
      </ul> }
      <button className="main-btn mt-10" onClick={() => {
              setShow(true);
            }}>
        Participer
      </button>
     {/* <FormModal idDemande={1} show={show} onHide={() => {setShow(false)}}/>  */}
     <FormModal idDemande={idDemande} show={show} onHide={() => {setShow(false)}}/> 
    </div>
  );
};

const ProjectDetailsSidebar = ({idDemande}) => {
  const [besoinsFinanciers,setBesoinsFinanciers]= useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        // console.log("besoins financiers, idDemandeAide passé en props ::::: " + idDemande)
        const response = await fetch("http://localhost:8080/getBesoinFinancierByDemande/"+idDemande);
        if (!response.ok) {
          // throw new Error(`Erreur HTTP! Statut : ${response.status}`);
        }
        const data = await response.json();
        setBesoinsFinanciers(data);
      } catch (error) {
        // console.error("Erreur lors de la recherche de la demande d'aide :", error);
      } finally {
        // console.log("BesoinsFinanciers:::::::::::::", besoinsFinanciers);
      }
    };
    fetchData();
  },[idDemande]);

  return (
    <div className="project-details-sidebar">
 
      {besoinsFinanciers.map((besoinFinancier) => (
        <ProjectDetailsPark perk={besoinFinancier} key={besoinFinancier.idBesoin} idDemande={idDemande}/>
      ))}
    </div>
  );
};

export default ProjectDetailsSidebar;
