import { categoriesSection } from "@/data/categories";
import React from "react";
import { Col, Container, Image, Row } from "react-bootstrap";
const { bg, tagline, title, text, categoriesUser, signIn, categories } = categoriesSection;
import { motion } from "framer-motion";
import { AccessibleOutlined } from "@mui/icons-material";
import { Diversity3Outlined } from "@mui/icons-material";

const CategoriesBoxItem = ({ categories = [] }) => {
  return (
    <div className="categories-box-item">
      {categories.map(({ id, icon, title }) => (
        <div key={id} className="item">
          <a href="#">
            <i className={icon}></i> <br />
            <span> {title} </span>
          </a>
        </div>
      ))}
    </div>
  );
};

const Categories = () => {
  return (
    <section
      className="categories-area pt-50 pb-50"
      // style={{ backgroundImage: `url(${bg.src})` }}
      // style={{ backgroundColor:"#fff" }}
    >
      <Container>
        <Row className="align-items-center text-center pt-25 pb-25">
          <span> {tagline} </span> <h3 className="title"> {title} </h3>
        </Row>
        <Row className="align-items-center">
          <Col lg={5}>
            <div className="categories-content">
              <p> {text} </p>
              {/* <div className="item d-flex align-items-center">
                <div className="thumb">
                  <Image src={categoriesUser.src} alt="" />
                </div>
                <Image src={signIn.src} alt="singin" />
              </div> */}
            </div>
          </Col>
          <Col lg={7}>
            <div className="categories-box">
              {/* <CategoriesBoxItem categories={categories.slice(0, 2)} /> */}
              {/* <CategoriesBoxItem categories={categories.slice(3)} /> */}

              <div className="categories-box-item">
                <motion.div className="item"
                  whileHover={{scale:1.2}}
                >
                  <a href="/howtowork">
                    <AccessibleOutlined sx={{fontSize:80}}/><br />
                    <span>Bénéficiaire</span>
                  </a>
                </motion.div>

                <motion.div className="item" whileHover={{scale:1.2}}>
                  <a href="/howtowork">
                    <Diversity3Outlined sx={{fontSize:80}}/><br />
                    <span>Donateur</span>
                  </a>
                </motion.div>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Categories;
