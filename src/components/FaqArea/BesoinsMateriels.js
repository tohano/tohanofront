import { CheckBox } from "@mui/icons-material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Modal, Row, Col, Container, FormCheck } from "react-bootstrap";
import {ToastContainer} from "react-toastify";

const ParticipeBesoinMaterielModal = ({ show, onHide, besoinMateriel, idDemande }) => {
  
  const [formData, setFormData] = useState({idBesoinMateriel:besoinMateriel.idBesoin});
  const {descriptionMateriel, quantiteMateriel, lieuLivraison, dateLivraison} = formData;
  

  function validationForm(){
    return descriptionMateriel && quantiteMateriel && lieuLivraison && dateLivraison;
  }
  const handleChange = (event)=>{
    const { name, value } = event.target;
    setFormData({...formData, [name]: value});
  }

  const handleSubmitForm = async (event)=>{
    event.preventDefault();
    const idDonateurAssociation = JSON.parse(sessionStorage.getItem('connectedUser'))

    idDemande = parseInt(idDemande);
    await axios.post(`http://localhost:8080/donationmat/ajout/${idDemande}/${idDonateurAssociation.id}`, formData)
    .then((response)=>{
      console.log("Lasa ve lay donées : ", response.data)
      setFormData({});
      onHide();
  })
    .catch((error)=>{console.log("Tsy lasa lay data ah,...")});
    console.log("formData : ",formData, " idDonateurAssociation : ", typeof(idDonateurAssociation.id))
  }
  
  return (
    <Modal
      show={show}
      size="lg"
      dialogClassName="modal-1200-600"
      contentClassName="modal-content-1200-600"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    ><form>
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">
          {besoinMateriel.intituleBesoinMateriels}
        </Modal.Title>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={onHide}
        ></button>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Col>
            <Row className="mb-3">
              <Col>
                <label htmlFor="" className="text-orange">
                  Déescription et état du : {besoinMateriel.intituleBesoinMateriels}
                </label>
                <div>
                  <textarea name="descriptionMateriel" rows={30} cols={40} value={descriptionMateriel} onChange={handleChange}></textarea>
                </div>
              </Col>
              <Col>
                <label htmlFor="" className="text-orange">
                  Quantité
                </label>
                <div className="input-box">
                  <input type="number" name="quantiteMateriel" value={quantiteMateriel} onChange={handleChange}/>
                </div>
              </Col>
            </Row>
            <Row>
              <Col>
                <label htmlFor="" className="text-orange">
                  Lieu de récuperation
                </label>
                <div className="input-box">
                  <input type="text" name="lieuLivraison" value={lieuLivraison} onChange={handleChange}/>
                </div>
              </Col>
              <Col>
                <label htmlFor="" className="text-orange">
                  Date de récuperation
                </label>
                <div className="input-box">
                  <input type="date" name="dateLivraison" value={dateLivraison} onChange={handleChange}/>
                </div>
              </Col>
            </Row>
          </Col>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <button className="main-btn" onClick={onHide}>
          Annuler
        </button>
        {validationForm() ?(
          <button type="submit" className="main-btn" onClick={handleSubmitForm}>Envoyer</button>
          ):(
          <button className="main-btn" disabled>Envoyer</button>

        )}
          
      </Modal.Footer>
    </form>
    </Modal>
  );
};

const BesoinMateriel = ({ besoinMateriel, current, handleCurrent, idDemande}) => {
  const active = current === besoinMateriel.idBesoin;
  const [showModal, setShowModal] = useState(false);
  const [totalDons, setTotalDons] = useState(0);

  const calculerTotalDons = async () => {
    try{
      const response = await axios.get(`http://localhost:8080/gettotaldonationbyidbesoinmateriel/${besoinMateriel.idBesoin}`);
      const data = await response.data;
      setTotalDons(data);
    }catch(error){
      // console.error("La requête axios a retourné une valeur neulle dans la fonction calculerTotalDons dans le fichier components/FaqArea/BesoinsMateriels.js ")
    }
  }

  const montrerStatusBesoin = (quantiteBesoin, quantiteDons) => {
    if(quantiteBesoin > quantiteDons){
      return "(En cours)";
    }
    return "(Rempli)";
  }

  useEffect(()=>{
    calculerTotalDons();
    // alert(totalDons);
  },[idDemande]);

  return (
    <div className={`accrodion overflow-hidden${active ? " active" : ""}`}>
      <Container className="accrodion-inner">
        <Row
          onClick={() => handleCurrent(besoinMateriel.idBesoin)}
          className="accrodion-title"
        >
          <h4>
            <span>
             
              {besoinMateriel.intituleBesoinMateriels}{" "}
              {totalDons} reçus sur {besoinMateriel.quantiteBesoinMateriel}
              {montrerStatusBesoin(besoinMateriel.quantiteBesoinMateriel, totalDons)}
            </span>
          </h4>
        </Row>
        <Row className={`accrodion-content${active ? "" : " d-none"}`}>
          <Col lg={6} md={6} className={`inner animated${active ? " fadeInUp" : ""}`}>
            <Button
              className="main-btn"
              onClick={() => {
                setShowModal(true);
              }}
            >
              Participer
            </Button>
            
          </Col>
         
        </Row>
      </Container>
      <ParticipeBesoinMaterielModal
        show={showModal}
        onHide={() => {
          setShowModal(false);
        }}
        besoinMateriel={besoinMateriel}
        idDemande ={idDemande}
      />
    </div>
  );
};

const BesoinsMateriels = ({ idDemande, className = "" }) => {
  const [current, setCurrent] = useState(1);
  const [besoinMaterielLists, setBesoinMaterielLists] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        // console.log(
        //   "besoins materiels id demande aide :::::::::::::::" + idDemande
        // );
        const response = await fetch(
          "http://localhost:8080/getBesoinsMaterielDuDemande/" + idDemande
        );
        if (!response.ok) {
          // throw new Error(`Erreur HTTP! Statut : ${response.status}`);
        }
        const data = await response.json();
        setBesoinMaterielLists(data);
      } catch (error) {
        // console.error(
        //   "Erreur lors de la recherche de la demande d'aide :",
        //   error
        // );
      } finally {
        // console.log("Besoins materiels:::::::::::::", besoinMaterielLists);
      }
    };
    fetchData();
  },[idDemande]);

  const handleCurrent = (current) => {
    setCurrent(current);
  };

  return (
    <div className={`faq-accordion overflow-hidden ${className}`}>
      <div
        className={`accrodion-grp${
          !className ? " animated fadeInLeft" : ""
        } faq-accrodion`}
      >
        {besoinMaterielLists.map((besoinMateriel) => (
          <BesoinMateriel idDemande={idDemande}
            besoinMateriel={besoinMateriel}
            key={besoinMateriel.idBesoin}
            current={current}
            handleCurrent={handleCurrent}
          />
        ))}
      </div>
    </div>
  );
};

export default BesoinsMateriels;
