import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";

const formModal = ({ show, onHide }) => {
  return (
    <Modal
      show={show}
      size="lg"
      dialogClassName="modal-1200-600"
      contentClassName="modal-content-1200-600"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">Titre</Modal.Title>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={onHide}
        ></button>
      </Modal.Header>
      <Modal.Body>
        <Container>Bonjour modal</Container>
      </Modal.Body>
      <Modal.Footer>
        <button className="main-btn" onClick={onHide}>
          Annuler
        </button>
        <button type="submit" className="main-btn">
          Valider
        </button>
      </Modal.Footer>
    </Modal>
  );
};

const BesoinFinancier = ({ besoinFinancier, current, handleCurrent }) => {
  const [show, setShow] = useState(false);
  const active = current === besoinFinancier.idBesoin;
  const montant = ((besoinFinancier.montantDemandee).toString()).replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  return (
    <div className={`accrodion overflow-hidden${active ? " active" : ""}`}>
      <div className="accrodion-inner">
        <div onClick={() => handleCurrent(besoinFinancier.idBesoin)} className="accrodion-title">
        
          <h4>
            <span>{besoinFinancier.idBesoin}.</span> {montant} Ar
          </h4>
        </div>
        <div className={`accrodion-content${active ? "" : " d-none"}`}>
          <div className={`inner animated${active ? " " : "fadeInUp"}`}>

            <Button className="main-btn" onClick={() => {
              setShow(true)
            }}>Participer 2</Button>
          </div>
        </div>
      </div>
      <formModal show={show} onHide={() => {setShow(false)}}/>
    </div>
  );
};

const BesoinsFinanciers = ({ idDemandeAide, className = "" }) => {
  const [current, setCurrent] = useState(1);
  const [besoinsFinanciers,setBesoinsFinanciers]= useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        console.log("besoins financiers, idDemandeAide passé en props ::::: " + idDemandeAide)
        const response = await fetch("http://localhost:8080/getBesoinFinancierByDemande/"+idDemandeAide);
        if (!response.ok) {
          throw new Error(`Erreur HTTP! Statut : ${response.status}`);
        }
        const data = await response.json();
        setBesoinsFinanciers(data);
      } catch (error) {
        console.error("Erreur lors de la recherche de la demande d'aide :", error);
      } finally {
        console.log("BesoinsFinanciers:::::::::::::", besoinsFinanciers);
      }
    };
    fetchData();
  },[]);

  const handleCurrent = (current) => {
    setCurrent(current);
  };

  return (
    <div className={`faq-accordion overflow-hidden ${className}`}>
      <div
        className={`accrodion-grp${
          !className ? " animated fadeInLeft" : ""
        } faq-accrodion`}
      >
        {besoinsFinanciers.map((besoinFinancier) => (
          <BesoinFinancier
            besoinFinancier={besoinFinancier}
            key={besoinFinancier.idBesoin}
            current={current}
            handleCurrent={handleCurrent}
          />
        ))}
      </div>
    </div>
  );
};

export default BesoinsFinanciers;
