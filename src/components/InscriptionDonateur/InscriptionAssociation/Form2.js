import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Title from "../../Reuseable/Title";
import Link from "next/link";

export default function Step2({ formData, setFormData, handleFormSubmit, handlePrevious }) {
    const { objetAssociation, nombreAdherentAssociation, presidentAssociation, adresse, contact, centreInteretAssociation} = formData;


    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    }

    return (
        <section className="contact-form-area">
            <Container>
                <Row className="justify-content-center">
                    <Col lg={8}>
                        <Title title="Informations de l'association" tagline="Etape 2/2 inscription de votre association " className="text-center" />
                    </Col>
                </Row>
                <Row className="justify-content-center">
                    <Col lg={8}>
                        <div className="conact-form-item">
                            <Row>
                                {/* eto voloany */}
                                <Col lg={6} md={6}>
                                    <div className="input-box mt-20">
                                        <h5 className='text-orange'>Objet de l'association</h5>
                                        <input
                                            type="text"
                                            name="objetAssociation"
                                            placeholder="Quel est le but de l'association?"
                                            value={objetAssociation}
                                            onChange={ e => handleChange(e) }
                                        />
                                    </div>
                                </Col>
                                <Col lg={6} md={6}>
                                    <div className="input-box mt-20">
                                        <h5 className='text-orange'>Nombre d'adhérant</h5>
                                        <input
                                            type="number"
                                            name="nombreAdherentAssociation"
                                            placeholder="Vous comptez combien d'adhérents?"
                                            value={nombreAdherentAssociation}
                                            onChange={ e => handleChange(e) }
                                        />
                                    </div>
                                </Col>
                                {/* eto farany */}
                                <Col lg={6} md={6}>
                                    <div className="input-box mt-20">
                                        <h5 className='text-orange'>Président de l'association</h5>
                                        <input
                                            type="text"
                                            name="presidentAssociation"
                                            placeholder="Qui est le président?"
                                            value={presidentAssociation}
                                            onChange={ e => handleChange(e) }
                                        />
                                    </div>
                                </Col>
                                <Col lg={6} md={6}>
                                    <div className="input-box mt-20">
                                        <h5 className='text-orange'>Adresse de votre association</h5>
                                        <input
                                            type="text"
                                            name="adresse"
                                            placeholder="Adresse de l'association?"
                                            value={adresse}
                                            onChange={ e => handleChange(e) }
                                        />
                                    </div>
                                </Col>
                                <Col lg={6} md={6}>
                                    <div className="input-box mt-20">
                                        <h5 className='text-orange'>Télephone</h5>
                                        <input
                                            type="text"
                                            name="contact"
                                            placeholder="Comment vous contacter?"
                                            value={contact}
                                            onChange={ e => handleChange(e) }
                                        />
                                    </div>
                                </Col>
                                <Col lg={6} md={6}>
                                    <div className="input-box mt-20">
                                        <h5 className='text-orange'>Centre d'intérêt</h5>
                                        <input
                                            type="text"
                                            name="centreInteretAssociation"
                                            placeholder="Centre d'intérêt de l'association?"
                                            value={centreInteretAssociation}
                                            onChange={ e => handleChange(e) }
                                        />
                                    </div>
                                </Col>
                                <Col lg={12}>
                                    {/* <Col lg={6}></Col> */}
                                    <div className="input-box mt-20 text-center">
                                        <button className="main-btn" onClick={handlePrevious}>Précédent</button>
                                        <button className="main-btn ml-140" onClick={handleFormSubmit}>Valider</button>

                                        <p className="text-center">Vous avez déjà un compte? connectez-vous <Link
                                            href="/login" className="nav-link"> ici </Link></p>
                                    </div>
                                </Col>

                            </Row>
                        </div>
                        <p className="form-message"></p>
                    </Col>
                </Row>
            </Container>
        </section>
)
}




