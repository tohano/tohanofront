import Form1 from "@/components/InscriptionDonateur/InscriptionAssociation/Form1";
import Form2 from "@/components/InscriptionDonateur/InscriptionAssociation/Form2";
import { useState } from "react";
import axios from 'axios';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useRouter } from "next/router";

const notify = () => {
        toast.success('Inscription réussie, vous serez redirigé automatiquement dans 3 secondes', {
          position: "top-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "colored",
          });
    };
export default function FormulaireAssociation() {
    const router = useRouter()
    const [currentStep, setCurrentStep] = useState(1);
    const [formData, setFormData] = useState({
        objetAssociation:'',
        pseudo: '',
        mail: '',
        password: '',
        passwordConfirmation: '',
    });

    const handleNext = () => {
        setCurrentStep(currentStep + 1);
        // console.log('Données du formulaire 1 soumises :', formData);
    };

    const handlePrevious = () => {
        setCurrentStep(currentStep - 1);
    };

    const handleFormSubmit = async (e) => {
        //alert(JSON.stringify(formData));
        // Ajoutez ici la logique pour envoyer les données du formulaire au serveur
        e.preventDefault();
        
        await axios.post("http://localhost:8080/inscription/donateurassociation", formData)
            .then(function (response) {
                // console.log("Post io" + response.data)
                let user = {id:response.data,userType:'donateur'};
                notify()
                if (typeof window !== 'undefined') {
                // Vérification de la disponibilité de sessionStorage
                    sessionStorage.setItem('connectedUser', JSON.stringify(user));
                }
                setTimeout(()=>{router.push('/homeDonateur')},2000)
                
            }).catch(function (error) {
                // console.error("Misy diso ooo" + error)
            });
        // console.log('Données du formulaire 1 + 2soumises :', formData);
    };

    const renderStep = () => {
        switch (currentStep) {
            case 1:
                return (

                    <Form1
                        formData={formData}
                        setFormData={setFormData}
                        handleNext={handleNext}
                    />

                );
            case 2:
                return (
                    <Form2
                        formData={formData}
                        setFormData={setFormData}
                        handlePrevious={handlePrevious}
                        handleFormSubmit={handleFormSubmit}
                    />
                );
            default:
                return null;
        }
    };

    return (
        <>
            <form className="contact-form-area mt-150" onSubmit={handleFormSubmit}>
                {renderStep()}
                
                    <ToastContainer
                        position="top-right"
                        autoClose={5000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                        theme="colored"
                        />
            </form>

        </>
    );
}
