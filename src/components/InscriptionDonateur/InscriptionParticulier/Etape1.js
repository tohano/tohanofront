
import React, {useState} from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Title from "../../Reuseable/Title";
import Link from "next/link";

export default function Step1({ formData, setFormData, handleNext }) {
    const { pseudo, mail, motDePasse } = formData;
    const [confirmPassword, setConfirmPassword] = useState('');
    const [passwordsMatch, setPasswordsMatch] = useState(true);
    const emailRegex = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    const [emailInputClassName, setEmailInputClassName] = useState('');


    const handleChange = (e) => {
        const { name, value } = e.target;

        if (name === 'mail') {
            const isValidEmail = emailRegex.test(value);
            setFormData({ ...formData, [name]: value });

            if (isValidEmail) {
                setEmailInputClassName('');
            } else {
                setEmailInputClassName('invalid-input');
            }
        } else if (name === 'motDePasse') {
            setFormData({ ...formData, [name]: value });

            if (name === 'password2') {
                if(value != motDePasse){}
            }
        } else {
            setFormData({ ...formData, [name]: value });
        }
    };

    const validateStep1 = () => {
        let valide = true;
        if(!pseudo || !mail || !motDePasse || !emailRegex.test(mail) || !confirmPassword) {
            valide = false;
        }
        //console.log(!emailRegex.test(mail))
        return valide;
    }

    const handleConfirmPasswordChange = (event) => {
        setConfirmPassword(event.target.value);
        setPasswordsMatch(motDePasse === event.target.value);
    };


    return (
        <section className="contact-form-area">
            <Container>
                <Row className="justify-content-center">
                    <Col lg={8}>
                        <Title title="Identifiants " tagline="Etape 1/2 du particulier" className="text-center"  />
                    </Col>
                </Row>
                <Row className="justify-content-center">
                    <Col lg={8}>
                        <div className="conact-form-item">
                            <Row>
                                {/* eto voloany */}
                                <Col lg={6} md={6}>
                                    <div className="input-box mt-20">
                                        <h5 className='text-orange'>Pseudo</h5>

                                        <input
                                            type="text"
                                            name="pseudo"
                                            placeholder="Pseudo"
                                            value={pseudo}
                                            onChange={ e => handleChange(e) }
                                            // style={{border: '1px solid red'}}
                                        />
                                    </div>
                                </Col>
                                {/* eto farany */}
                                <Col lg={6} md={6}>
                                    <div className={`input-box mt-20`}>
                                        <h5 className='text-orange'>Pseudo</h5>

                                        <input
                                            className=' `${emailInputClassName}`'
                                            type="mail"
                                            name="mail"
                                            placeholder="Email"
                                            value={mail}
                                            onChange={ e => handleChange(e) }

                                        />
                                    </div>
                                </Col>
                                <Col lg={6} md={6}>
                                    <div className="input-box mt-20">
                                        <h5 className='text-orange'>Mot de passe</h5>
                                        <input
                                            type="password"
                                            name="motDePasse"
                                            placeholder="Mot de passe"
                                            value={motDePasse}
                                            onChange={ e => handleChange(e) }
                                        />
                                    </div>
                                </Col>
                                <Col lg={6} md={6}>
                                    <div className={`input-box mt-20`}>
                                        <h5 className='text-orange'>Confirmation mot de passe</h5>
                                        <input
                                            type="password"
                                            name="password2"
                                            placeholder="Confirmer votre mot de passe"
                                            onChange={e => { handleConfirmPasswordChange(e) }}
                                        />
                                        <br />
                                        {!passwordsMatch && <div style={{color:'red'}}>Les mots de passe ne correspondent pas.</div>}
                                    </div>
                                </Col>
                                <Col lg={12}>
                                    <div className="input-box mt-20 text-center">
                                        {validateStep1() && passwordsMatch==true ? (
                                            <button className="main-btn" onClick={handleNext}>
                                                Suivant
                                            </button>
                                        ) : (
                                            <button className="main-btn" disabled>
                                               Suivant
                                            </button>
                                        )}
                                    </div>
                                    <p className="text-center mt-4">Vous avez déjà un compte? connectez-vous <Link href="/login" className="nav-link">ici</Link></p>
                                </Col>
                            </Row>
                        </div>

                        <p className="form-message"></p>
                    </Col>
                </Row>
            </Container>
        </section>
    );
}