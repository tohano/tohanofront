import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Title from "../../Reuseable/Title";
import Link from "next/link";

export default function Form2({ formData, setFormData, handlePrevious }) {
    const {nomParticulier, prenomParticulier, adresseParticulier, contactParticulier} = formData;

    const handleChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    };

        return (
            <section className="contact-form-area">
                <Container>
                    <Row className="justify-content-center">
                        <Col lg={16}>
                            <Title title="Informations personnelles" tagline="Etape 2/2 du particulier" className="text-center" />
                        </Col>
                    </Row>
                    <Row className="justify-content-center">
                        <Col lg={8}>
                            <form className="contact-form">
                                <div className="conact-form-item">
                                    <Row>
                                        {/* eto voloany */}
                                        <Col lg={6} md={6}>
                                            <div className="input-box mt-20">
                                                <h5 className='text-orange'>Votre nom</h5>
                                                <input
                                                    type="text"
                                                    name="nomParticulier"
                                                    placeholder="Nom"
                                                    value={nomParticulier}
                                                    onChange={ e => handleChange(e) }
                                                />
                                            </div>
                                        </Col>
                                        <Col lg={6} md={6}>
                                            <div className="input-box mt-20">
                                                <h5 className='text-orange'>Votre prénom</h5>
                                                <input
                                                    type="text"
                                                    name="prenomParticulier"
                                                    placeholder="Prénom"
                                                    value={prenomParticulier}
                                                    onChange={ e => handleChange(e) }
                                                />
                                            </div>
                                        </Col>
                                        {/* eto farany */}
                                        <Col lg={6} md={6}>
                                            <div className="input-box mt-20">
                                                <h5 className='text-orange'>Votre adresse</h5>
                                                <input
                                                    type="text"
                                                    name="AdresseParticulier"
                                                    placeholder="Adresse"
                                                    value={adresseParticulier}
                                                    onChange={ handleChange}
                                                />
                                            </div>
                                        </Col>
                                        <Col lg={6} md={6}>
                                            <div className="input-box mt-20">
                                                <h5 className='text-orange'>Votre contact</h5>
                                                <input
                                                    type="text"
                                                    name="contactParticulier"
                                                    placeholder="Contact"
                                                    value={contactParticulier}
                                                    onChange={ handleChange }
                                                />
                                            </div>
                                        </Col>
                                        <Col lg={16}>
                                            {/* <Col lg={6}></Col> */}

                                            <div className="input-box mt-20 text-center">
                                                <button onClick={handlePrevious} className="main-btn main-btn-2">Précédent</button>
                                                <button className="main-btn ml-120" type="submit">Valider</button>
                                                
                                                <p className="text-center mt-25">Vous avez déjà un compte? connectez-vous <Link href="/Login" className="nav-link"> ici </Link></p>

                                            </div>
                                        </Col>

                                    </Row>
                                </div>
                            </form>
                        </Col>
                    </Row>
                </Container>
            </section>
        );

}

