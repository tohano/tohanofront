import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { ctaAreaDonateur } from "@/data/ctaAreaDonateur";

const { bg, title } = ctaAreaDonateur;

const CtaAreaDonateur = () => {
  return (
    <section className="cta-area" style={{ margin: '100px 50px' }}>
      <Container>
        <Row>
          <Col lg={12}>
            <div
              className="cta-item bg_cover"
              style={{ backgroundImage: `url(${bg.src})` }}
            >
              <div className="cta-content d-block d-lg-flex justify-content-between align-items-center">
                <h3 className="title">{title}</h3>
                <a className="tohano-btn" href="#" style={{ display: 'block', textAlign: 'center' }}>
                faire un donation
                </a>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default CtaAreaDonateur;
