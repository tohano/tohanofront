import bg from "@/images/categories-bg.jpg";
import categoriesUser from "@/images/categories-user.png";
import signIn from "@/images/singin.png";

export const categoriesSection = {
  bg,
  tagline: "Vous êtes un malade ou vous voulez devenir un donateur ?",
  title: "Principales catégories",
  text: "Notre objectif est de fournir un accès aux soins de santé de qualité aux patients malgaches, indépendamment de leur lieu de résidence ou de leur situation financière. À travers la plateforme TOHANO, nous visons à faciliter la mise en relation entre les patients et les associations locales, leur offrant ainsi des services médicaux, des consultations spécialisées, des médicaments et d'autres ressources liées à leur maladie. ",
  categoriesUser,
  signIn,
  categories: [
    {//flaticon-networking
      id: 1,
      icon: "flaticon-gift",
      title: "Bénéficiaire",
    },
    {
      id: 2,
      icon: "flaticon-photograph",
      title: "Donnateur",
    },
    {
      id: 3,
      icon: "flaticon-translation",
      title: "Education",
    },
    {
      id: 4,
      icon: "flaticon-stethoscope",
      title: "Medical",
    },
    {
      id: 5,
      icon: "flaticon-skincare",
      title: "Fashion",
    },
    {
      id: 6,
      icon: "flaticon-design",
      title: "Design",
    },
  ],
};
