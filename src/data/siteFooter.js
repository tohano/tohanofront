import bg from "@/images/footer-bg.jpg";
import shape from "@/images/footer-shape.png";
import logo from "@/images/LogoTohano_LogoTurquoiseTohanoBanniere.png";
//logo-2 
export const socials2 = [
  {
    id: 1,
    icon: "fa fa-twitter",
    href: "#",
  },
  {
    id: 2,
    icon: "fa fa-facebook-official",
    href: "#",
  },
  {
    id: 3,
    icon: "fa fa-pinterest",
    href: "#",
  },
  {
    id: 4,
    icon: "fa fa-instagram",
    href: "#",
  },
];

const footerData = {
  logo,
  bg,
  socials: socials2,
  text: "“Donner de l'espoir, changer des vies”",
  text2:
    "Inscrivez-vous pour nos dernières nouvelles et articles. Nous ne vous enverrons pas de spams.",
  shape,
  links: [
    {
      id: 1,
      text: "A propos",
      href: "/about",
    },
    {
      id: 2,
      text: "Comment ça marche",
      href: "/team",
    },
    {
      id: 3,
      text: "Publication",
      href: "/case",
    },
    {
      id: 4,
      text: "Success Stories",
      href: "/blog",
    },
    {
      id: 5,
      text: "Contact",
      href: "/contact",
    },
    {
      id: 6,
      text: "Maladie",
      href: "/about",
    },
    {
      id: 7,
      text: "Cancer",
      href: "/about",
    },
    {
      id: 8,
      text: "A V C",
      href: "/about",
    },
    {
      id: 9,
      text: "Maladie oculaire",
      href: "/about",
    },
    {
      id: 10,
      text: "Autres",
      href: "/about",
    },
  ],
  author: "Tohano",
  year: new Date().getFullYear(),
};

export default footerData;
