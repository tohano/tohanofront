import image1 from "@/images/malade/malformation.jpg";
import image2 from "@/images/malade/tumeur.jpg";

const urgentHelpRequests = [
    {
        id:1,
        image: image1,
        title: "Appel à l'aide pour traiter une malformation anorectale"
    },
    {
        id:2,
        image:image2,
        title: "Traitement d'une tumeur au cerveau pour mon fils de 4 ans"
    },
];


export const urgentHelpRequestsArea = {
    tagline: "Des pesonnes qui ont besoins de vous",
    title: "Nos Demandes d'Aides en Urgences",
    urgentHelpRequests,
};