export const howdonateur = {
  title: "Faire des donations anonymes",
  tagline: "",
  title2: "Répondre à une demande d'aide",
  items: [
    {
      id: 1,
      title: "Voir les demandes",
      text: "Vous pouvez aller dans l’onglet “Voir les demandes ” pour regarder toutes les demandes d’aide, cliquez dessus et vous allez voir tous les demandes d'aides.",
    },
    {
      id: 2,
      title: "Faire une recherche",
      text: "Vous pouvez utiliser la barre de recherche si vous souhaitez aider un malade en particulier selon vos critères, sa localisation ou autres.",
    },
    {
      id: 3,
      title: "Choisir le malade à aider",
      text: "Une fois que vous avez trouvé le malade de votre choix, cliquez sur “Détails” pour voir les informations sur ce malade, ses besoins, etc. Puis cliquez sur “Répondre”.",
    },
    {
      id: 4,
      title: "Pour une donation matérielle",
      text: "Lorsque vous faites un don matériel, on vous demandera de fixer un lieu et une heure de rendez-vous pour qu’on puisse récupérer votre don. Vous pourrez aussi le remettre aux malades si tel est votre choix.",
    },
    {
      id: 5,
      title: "Pour une donation financière",
      text: "Pour faire un don financier, entrez le montant que vous souhaitez, l’argent sera transféré via Mvola",
    },
    {
      id: 6,
      title: "Donation anonyme",
      text: "Vous êtes libre de rester anonyme ou non, veuillez cocher sur “Anonyme” pour le rester. ",
    },
  ],
};
