import image1 from "@/images/assos/ASSPO.jpg";
import image2 from "@/images/assos/Fitia.jpg";
import image3 from "@/images/assos/AST.jpg";
import image4 from "@/images/assos/YMCA.jpg";
import image5 from "@/images/assos/Assos_mitafa.jpg";
import image6 from "@/images/assos/Assos_Zarasoa.jpg";
import maladieOsseuse from "@/images/malade/maladieOsseuse370x323.jpg"
import cancerCerveau from "@/images/malade/cancerCerveau370x323.jpg"
import flag from "@/images/flag.png";
import info from "@/images/info.jpg";
import bg1 from "@/images/project-bg-1.jpg";
import bg2 from "@/images/project-bg-2.jpg";
import projectDetails1 from "@/images/project-details-1.jpg";
import projectDetails2 from "@/images/project-details-2.jpg";
import thumb2 from "@/images/project-details-thumb.jpg";
import projectInfo from "@/images/project-info.jpg";
import projectPerk from "@/images/project-perk.jpg";
import thumb from "@/images/single-project-thumb.jpg";
import { faqs } from "./faqArea";


export const socials3 = [
  {
    id: 1,
    icon: "fa fa-facebook-square",
    href: "#",
  },
  {
    id: 2,
    icon: "fa fa-twitter",
    href: "#",
  },
  {
    id: 3,
    icon: "ffa fa-pinterest-p",
    href: "#",
  },
  {
    id: 4,
    icon: "fa fa-instagram",
    href: "#",
  },
];

const projects = [
  {
    id: 1,
    image: image1,
    category: "Design",
    date: "20 Days Left",
    title: "A S S P O",
    goal: "3600.00",
    raised: 23,
  },
  {
    id: 2,
    image: image2,
    category: "Design",
    date: "20 Days Left",
    title: "Association Fitia",
    goal: "3600.00",
    raised: 23,
  },
  {
    id: 3,
    image: image3,
    category: "Design",
    date: "20 Days Left",
    title: "A S T",
    goal: "3600.00",
    raised: 23,
  },
  {
    id: 4,
    image: image4,
    category: "Design",
    date: "20 Days Left",
    title: "Y M C A",
    goal: "3600.00",
    raised: 23,
  },
  {
    id: 5,
    image: image5,
    category: "Design",
    date: "20 Days Left",
    title: "Association Mitafa",
    goal: "3600.00",
    raised: 23,
  },
  {
    id: 6,
    image: image6,
    category: "Design",
    date: "20 Days Left",
    title: "Association Zarasoa",
    goal: "3600.00",
    raised: 23,
  },
];

export const projectsArea = {
  tagline: "Des associations qui sont inscrites sur la plateforme",
  title: "Elles peuvent vous aider",
  projects,
};

export const exploreProjects = {
  tagline: "Businesses You Can Back",
  title: "Explore Projects",
  projects: [
    {
      id: 1,
      image: "project-1.jpg",
      tagline: "Design",
      date: "20 Days Left",
      title: "The Power Bank that is Always Charged",
      raised: 23,
    },
    {
      id: 2,
      image: "project-2.jpg",
      tagline: "Design",
      date: "20 Days Left",
      title: "The Power Bank that is Always Charged",
      raised: 23,
    },
    {
      id: 3,
      image: "project-3.jpg",
      tagline: "Design",
      date: "20 Days Left",
      title: "The Power Bank that is Always Charged",
      raised: 23,
    },
    {
      id: 4,
      image: "project-4.jpg",
      tagline: "Design",
      date: "20 Days Left",
      title: "The Power Bank that is Always Charged",
      raised: 23,
    },
    {
      id: 5,
      image: "project-5.jpg",
      tagline: "Design",
      date: "20 Days Left",
      title: "The Power Bank that is Always Charged",
      raised: 23,
    },
    {
      id: 6,
      image: "project-7.jpg",
      tagline: "Design",
      date: "20 Days Left",
      title: "The Power Bank that is Always Charged",
      raised: 23,
    },
  ],
};

export const helpRequests = [
  {
    id: 1,
    image: maladieOsseuse,
    tagline: "demande d'aide",
    date: "20 Days Left",
    title: "Aidez mon enfant à avoir des os en bonne santé",
    raised: 23,
  },
  {
    id: 2,
    image: cancerCerveau,
    tagline: "Demande d'aide",
    date: "20 Days Left",
    title: "Luttons ensemble contre le cancer",
    raised: 23,
  },
];

export const projectArea = [
  {
    id: 1,
    bg: bg1,
    title: "Ready? Explore",
    text: "There are not is many variations of passages of lorem available.",
    linkText: "Discover projects",
  },
  {
    id: 2,
    bg: bg2,
    title: "Feeling Inspired?",
    text: "There are not is many variations of passages of lorem available.",
    linkText: "Join our team",
    btnClassName: " main-btn-2",
    className: " project-inspired",
  },
];

console.log("Aujourd'hui : " + new Date().getDay());
const days = 18 - new Date().getDate(); 
export const projectDetailsArea = {
  thumb,
  flag,
  tagline: "post-opératoire",
  country: "Madagascar",
  title: "Besoin d'aide pour des traitements post-opératoires",
  pledged: 2203,
  backers: 68,
  daysLeft: days,
  raised: 23,
  goal: "3600.00",
  socials: socials3,
};

export const projectDetailsTabBtns = [
  {
    id: "pills-home",
    name: "Description",
  },
  {
    id: "pills-profile",
    name: "Besoins Financiers",
  },
  {
    id: "pills-contact",
    name: "Besoins Matérielles",
  }
];


export const helpRequestsTbns = [
  {
    id: "description",
    name: "Description",
  },
  {
    id: "besoins",
    name: "Besoins",
  },
  {
    id: "participation",
    name: "Votre participation",
  },
];


export const projectDetailsStory = {
  id: "description",
  text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vulputate sed mauris vitae pellentesque. Nunc ut ullamcorper libero. Aenean fringilla mauris quis risus laoreet interdum. Quisque imperdiet orci in metus aliquam egestas. Fusce elit libero, imperdiet nec orci quis, convallis hendrerit nisl. Cras auctor nec purus at placerat.",
  lists: [
    "Nsectetur cing mauris quis risus laoreet elit.",
    "Suspe ndisse dolor sit amet suscipit sagittis leo.",
    "Entum estibulum metus aliquam egestas dignissim posuere.",
    "If you are going to use a auctor nec purus passage.",
    "Lorem Ipsum on the tend to repeat.",
  ],
  image: thumb2,
  text2:
    "Nous nous adressons à vous aujourd'hui pour partager l'histoire de fatima, une jeune fille courageuse qui a récemment subi une amputation de la jambe. Malgré les défis auxquels elle est confrontée, elle reste forte et déterminée à surmonter les obstacles.Cependant, elle a besoin de notre aide. Les coûts médicaux, les soins post-opératoires, la rééducation et les prothèses peuvent être écrasants. Nous lançons donc cet appel à votre générosité pour aider fatima et sa famille à couvrir ces dépenses.Toute contribution, quelle qu'elle soit, sera grandement appréciée. Ensemble, nous pouvons faire une différence dans la vie de fatima et lui donner l'espoir et le soutien dont elle a besoin en ce moment.Merci pour votre générosité et votre soutien.",
  text3:
    "Nulla in ex at mi viverra sagittis ut non erat raesent nec congue elit. Nunc arcu odio, convallis a lacinia ut, tristique id eros. Suspendisse leo erat, pellentesque et commodo vel, varius in felis. Nulla mollis turpis porta justo eleifend volutpat. Cras malesuada nec magna eu blandit. Nam sed efficitur ante. Quisque lobortis sodales risus, eu dapibus dolor porta vitae. Vestibulum eu ex auctor, scelerisque velit sit amet, vehicula sapien.",
  items: [
    {
      id: 1,
      title: "2 béquilles",
      text: "Lorem Ipsum nibh vel velit auctor aliquet. Aenean sollic tudin, lorem is simply free text quis bibendum.",
    },
    {
      id: 2,
      title: "One Power Bank for Every Wrist Car",
      className: "mt-45 mb-50",
      text: "Lorem Ipsum nibh vel velit auctor aliquet. Aenean sollic tudin, lorem is simply free text quis bibendum.",
    },
  ],
  images: [projectDetails1, projectDetails2],
};

export const projectDetailsFaq = {
  id: "besoins",
  faqs,
};

export const projectDetailsUpdates = {
  id: "pills-contact",
  updates: [
    {
      id: 1,
      title: "This is the first update of our new project",
      info: {
        image: info,
        name: "Sarah Albert",
        date: "4 March, 2020",
      },
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vulputate sed mauris vitae pellentesque. Nunc ut ullamcorper libero. Aenean fringilla mauris quis risus laoreet interdum. Quisque imperdiet orci in metus aliquam egestas. Fusce elit libero, imperdiet nec orci quis, convallis hendrerit nisl. Cras auctor nec purus at placerat.",
      text2:
        "Quisque consectetur, lectus in ullamcorper tempus, dolor arcu suscipit elit, id laoreet tortor justo nec arcu. Nam eu dictum ipsum. Morbi in mi eu urna placerat finibus a vel neque. Nulla in ex at mi viverra sagittis ut non erat. Praesent nec congue elit.",
      image: "project-updates-thumb-1.jpg",
    },
    {
      id: 2,
      title: "This is the first update of our new project",
      info: {
        image: info,
        name: "Sarah Albert",
        date: "4 March, 2020",
      },
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vulputate sed mauris vitae pellentesque. Nunc ut ullamcorper libero. Aenean fringilla mauris quis risus laoreet interdum. Quisque imperdiet orci in metus aliquam egestas. Fusce elit libero, imperdiet nec orci quis, convallis hendrerit nisl. Cras auctor nec purus at placerat.",
      text2:
        "Quisque consectetur, lectus in ullamcorper tempus, dolor arcu suscipit elit, id laoreet tortor justo nec arcu. Nam eu dictum ipsum. Morbi in mi eu urna placerat finibus a vel neque. Nulla in ex at mi viverra sagittis ut non erat. Praesent nec congue elit.",
      image: "project-updates-thumb-2.jpg",
    },
  ],
};

export const besoins = [
  {
    id : 1,
    intitule : "béquilles",
    quantite : 2,
    estRempli : false,
    type : "materiel"
  },
  {
    id : 2,
    montant: 10000,
    estRempli : false,
    type : "financier"
  }
];


export const projectDetailsComments = {
  id: "pills-4",
  comments: [
    {
      id: 1,
      image: "comment-2.2.jpg",
      name: "Kevin Martin",
      date: "3 March, 2020",
      text: "Lorem Ipsum is simply dummy text of the rinting and typesetting been the industry standard dummy text ever sincer condimentum purus. In non ex at ligula fringilla lobortis.",
    },
    {
      id: 2,
      image: "comment-2.1.jpg",
      name: "Sarah albert",
      date: "3 March, 2020",
      text: "Lorem Ipsum is simply dummy text of the rinting and typesetting been the industry standard dummy text ever sincer condimentum purus. In non ex at ligula fringilla lobortis.",
    },
  ],
};

export const projectDetailsSidebar = {
  info: {
    image: projectInfo,
    name: "John Smith",
    backed: 20,
    text: "Aenean fringilla mauris quis risus laoreet interdum. Quisque imperdiet orci in metus aliquam egestas.",
  },
  perks: [
    {
      id: 1,
      image: projectPerk,
      sold: 50,
      off: 62,
      amount: "5,800",
      date: "November 2022",
      claimed: 23,
      totalClaimed: 30,
    },
    {
      id: 2,
      sold: 50,
      off: 62,
      amount: "5,800",
      date: "November 2022",
      claimed: 23,
      totalClaimed: 30,
    },
  ],
};

export const needsDetails = [
  {
    id: 1,
    amount: 10000.00,
    date: "18 Janvier 2024",
    recu: 2300.00,
    satisfait: false,
    type:"financier"
  },
  {
    id: 2,
    intitule: "Bequilles",
    date: "18 Janvier 2024",
    quantite: 4,
    recu: 1,
    satisfait: false,
    type:"materiel"
  },
];

export const similarProjects = {
  tagline: "D'autres personnes que vous pouvez aider",
  title: "Autres demandes",
  projects: projects.slice(0, 3),
};
