export const easyStepsArea = {
  tagline: "Étapes simples et faciles",
  title: "Comment ça marche?",
  text: "Un malade se connecte sur son compte et publie une démande d'aide. Une fois la demande d'aide validée par le modérateur, elle sera affichée sur la plateforme de Tohano et sera visible par tous les internautes. L'internaute pourra faire une donation de manière anonyme ou pas",
  steps: [
    {
      id: 1,
      image: "connect.jpg",
      icon: "flaticon-creativity",
      title: "Connection",
    },
    {
      id: 2,
      image: "posted.jpg",
      icon: "flaticon-invention",
      title: "Publication d'une demande d'aide",
    },
    {
      id: 3,
      image: "validation.jpg",
      icon: "flaticon-smartphone",
      title: "Attente et validation de la publication",
    },
    {
      id: 4,
      image: "gift.jpg",
      icon: "flaticon-nanotechnology",
      title: "Donation",
    },
  ],
};
