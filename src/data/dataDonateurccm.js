import image from "@/images/aboutdonateur.jpg";

const text =
  "Tohano permet à tout donateur qui souhaite soutenir financièrement ou matériellement des patients. Vous pouvez choisir des causes selon vos principes et moyens. Pas besoin d’inscription pour contribuer, et l’anonymat est possible. L’inscription est optionnelle pour devenir un parrain membre.";

const textInscription =
  "Pour s'inscrire comme donateur sur Tohano, allez à l'onglet \"S'inscrire\". Remplissez les formulaires de compte, cliquez sur \"Suivant\" pour entrer vos informations, puis \"Valider\" pour terminer l'inscription.";
const textDonation ="Sur Tohano, vous pouvez rechercher et sélectionner les patients que vous souhaitez aider. Une fois que vous avez trouvé le patient, cliquez sur \"Participer\" et choisissez le type d’aide que vous souhaitez apporter.";
const textProfil="Si vous êtes inscrit, vous pouvez mettre à jour vos informations en allant dans votre profil, cliquant sur \"Modifier\", apportant les modifications nécessaires, puis cliquant sur \"Valider\" pour sauvegarder les changements.";
export const dataDonateurccm = {
  tagline: "",
  title: "Comment faire une donation sur Tohano",
  image,
  text,
  pills: [
    {
      id: "pills-home",
      tagline: "inscription",
      text: textInscription,
    },
    {
      id: "pills-profile",
      tagline: "participer à un don",
      text :textDonation,
    },
    {
      id: "pills-contact",
      tagline: "gérer son profil",
      text:textProfil ,
    },
  ],
  
};
