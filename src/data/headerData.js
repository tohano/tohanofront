import icon from "@/images/header-icon.png";
import logo2 from "@/images/LogoTohano_LogoTurquoiseTohano.png";
import logo from "@/images/LogoTohano_LogoTurquoiseTohanoBanniere.png";
export const navInternaute = [
  {
    id: 1,
    name: "Accueil",
    href: "/"
  },
  {
    id: 2,
    name: "Demande d'aide",
    href: "/listHelpRequestMap",
  },
  {
    id: 3,
    name: "Comment ça marche",
    href: "/howtowork",
  }
  ,
  {
    id: 4,
    name: "Se connecter",
    href: "/login",
  },
  {
    id: 5,
    name: "S'inscrire",
    href: "",
    subNavItems: [
      { id: 6, name: "En tant que malade", href: "/signinPatient" },
      { id: 7, name: "En tant qu'Association", href: "/inscriptionAssociation" },
      { id: 8, name: "En tant que Particulier", href: "/inscriptionParticulier" },
    ],
  },
];
export const navAdmin = [
  {
    id: 1,
    name: "Accueil",
    href: "/",
  },
  {
    id: 2,
    name: "Demande d'aide",
    href: "/validationDemandes",
  },
  {
    id: 3,
    name: "Tableau de bord",
    href: "/Admin/admin-home",
  },
  {
    id: 4,
    name: "Utilisateurs",
    href: "/howtowork",
  },
  {
    id: 5,
    name: "Déconnexion",
    href: "/logout",
  },
];
export const navDonateur = [
  {
    id: 1,
    name: "Demande d'aide",
    href: "/listHelpRequestMap",
  },
  {
    id: 3,
    name: "Gestion de donations",
    href: "/Admin/donation",
  },
  {
    id: 5,
    name: "Mon Compte",
    href: "",
    subNavItems: [
      { id: 6, name: "Profil", href: "" },
      { id: 7, name: "Se déconnecter", href: "/logout" },
    ],
  }
];
export const navPatient = [
  {
    id: 1,
    name: "Accueil",
    href: "/maladeHome",
  },
  {
    id: 2,
    name: "Créer une demande",
    href: "/patient/addInfoRequest",
  },
  {
    id: 3,
    name: "Calendrier",
    href: "/howtowork",
  },
  {
    id: 4,
    name: "Mon Compte",
    href: "/howtowork",
  },
  {
    id: 5,
    name: "Déconnexion",
    href: "/logout",
  },
];

export const socials = [
  {
    id: 1,
    icon: "fa fa-facebook-square",
    href: "#",
  },
  {
    id: 2,
    icon: "fa fa-twitter",
    href: "#",
  },
  {
    id: 3,
    icon: "fa fa-instagram",
    href: "#",
  },
  {
    id: 4,
    icon: "fa fa-dribbble",
    href: "#",
  },
];

const headerData = {
  logo,
  logo2,
  icon,
  navInternaute,
  navAdmin,
  navDonateur,
  navPatient,
  email: "contact@tohano.com",
  phone: "+261 34 86 068 86",
  address: "Faravohitra Antananarivo, Madagascar",
  socials,
};

export default headerData;
