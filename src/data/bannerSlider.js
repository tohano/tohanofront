import banner3 from "@/images/banner-color-shadow.png";
import banner from "@/images/banner-line.png";
import banner1 from "@/images/shape/banner-line-1.png";
import banner2 from "@/images/shape/banner-line-2.png";

export const bannerSlider = [
  {
    id: 1,
    bg: "bbg8.jpg",
    text: "Donner de l'espoir, changer des vies",
    title: "Tohano Plateforme",
    banner,
  },
  {
    id: 2,
    bg: "bbg2.jpg",
    text: "Donner de l'espoir, changer des vies",
    title: "Tohano Plateforme",
    banner,
  },
  {
    id: 3,
    bg: "bbg4.jpg",
    title: "Join the Journey From Idea to Market",
    banner: banner3,
    banner1,
    banner2,
  },
  {
    id: 4,
    bg: "bbg5.jpg",
    title: "Join the Journey From Idea to Market",
    banner: banner3,
    banner1,
    banner2,
  },
  {
    id: 5,
    bg: "bbg6.jpg",
    title: "We help surface innovations in tech",
  },
  {
    id: 6,
    bg: "banner-bg-2.jpg",
    title: "We help surface innovations in tech",
  },
];
