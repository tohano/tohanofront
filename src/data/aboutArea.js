import thumb3 from "@/images/step.jpg";
import thumb1 from "@/images/about-thumb-1.jpg";
import thumb2 from "@/images/about-thumb-2.jpg";
import image from "@/images/about-thumb-3.jpg";

export const aboutArea = {
  thumb1,
  thumb2,
  tagline: "Ready to Go?",
  title: "We Help at Every Step From Concept to Market",
  text: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised.",
  lists: [
    "Nsectetur cing elit.",
    "Suspe ndisse suscipit sagittis leo.",
    "Entum estibulum dignissim posuere.",
    "If you are going to use a passage.",
    "Lorem Ipsum on the tend to repeat.",
  ],
  experience: 30,
  experienceText: "Years of \n Experience",
};

export const aboutAreaThree = {
  tagline: "Learn More About the Krowd",
  title: "Founded to Empower Innovation",
  image,
  videoId: "2TvWZEVf6go",
  aboutTitle:
    "We empower people to unite around ideas that matter to them and together make those ideas come to life.",
  items: [
    {
      id: 1,
      title: "Highest Success Rates",
      text: "Lorem Ipsum nibh vel velit auctor aliquet. Aenean sollic tudin, lorem is simply free text quis bibendum.",
    }
  ],
};

export const aboutIntroduction = {
  thumb: thumb3,
  tagline: "Comment créer une publication?",
  title: "Etapes pour créer votre publication :",
  count: 4, 
  text: "",
  items: [
    "Donner une description de votre demande",
    "De quoi ou de quel montant avez-vous besoin?",
    "Quand en avez-vous besoin?",
    "Où vous situez-vous?",
  ],
  bottomText: "étapes",
};
