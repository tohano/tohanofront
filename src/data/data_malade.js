export const malade_liste = [{
        idUtilisateur: 1,
        mail: "sitraka@gmail.com",
        motDePasse: "$2a$10$PsWNpNwuBmxzXPtaSWJgdulYmk0HcnzDEm4cXWUmpw1pgNm2NH5zG",
        pseudo: "sitraka",
        contact: "0325678945",
        adresse: "lot 2025",
        nomMalade: "sitraka",
        prenomMalade: "rakotonirina",
        dateNaissance: "1989-02-23",
        cinMalade: "201092365874",
        ficheMedicales: [],
        demandeAides: [{
                idDemandeAide: 1,
                titreDemandeAide: " l'ostéogenèse imparfaite,",
                descriptionDemandeAide: "Rakoto, un homme courageux qui vit avec l'ostéogenèse imparfaite, une maladie génétique qui se caractérise par une fragilité osseuse extrême. En raison de sa condition, il utilise un fauteuil roulant pour se déplacer",
                dateDemandeAide: "2024-01-12",
                dateLimite: "2024-01-31",
                lieu: "Bel'air, Ankadivato, 101 Antananarivo, Madagascar",
                longitude: 0,
                latitude: 0,
                statut: false,
                mediaLists: [{
                    idMedia: 1,
                    fichier: "images/59916618-f68d-49c7-b4c9-211d357aaab9.jpg",
                    fileName: "26.jpg",
                    typeMedia: "image/jpeg"
                }],
                besoinMaterielLists: [{
                    idBesoin: 1,
                    estRempli: false,
                    intituleBesoinMateriels: "chaise roulante",
                    quantiteBesoinMateriel: 1
                }],
                besoinFinancierLists: [{
                    idBesoin: 1,
                    estRempli: false,
                    montantDemandee: 10000000
                }],
                donationFinancierLists: [],
                donationMaterielLists: []
            },
            {
                idDemandeAide: 2,
                titreDemandeAide: " l'ostéogenèse imparfaite,",
                descriptionDemandeAide: "Malgré les défis auxquels il est confronté, Rakoto reste fort et déterminé à vivre pleinement sa vie. Cependant, il a besoin de notre aide. Les coûts associés à sa condition, y compris les soins médicaux, les adaptations à son",
                dateDemandeAide: "2024-01-12",
                dateLimite: "2024-01-12",
                lieu: "Bel'air, Ankadivato, 101 Antananarivo, Madagascar",
                longitude: 0,
                latitude: 0,
                statut: false,
                mediaLists: [{
                    idMedia: 2,
                    fichier: "images/80acd2cb-eeee-49ea-96cf-7b4dad22ee47.jpg",
                    fileName: "25.jpg",
                    typeMedia: "image/jpeg"
                }],
                besoinMaterielLists: [],
                besoinFinancierLists: [],
                donationFinancierLists: [],
                donationMaterielLists: []
            }
        ],
        retours: []
    },
    {
        idUtilisateur: 2,
        mail: "fatima@gmail.com",
        motDePasse: "$2a$10$fdi17P4f35IHp2wqo5Sjfebq/5pJ4zWnybZ7Tn1eV/bVDgzPEcrpW",
        pseudo: "fatima",
        contact: "0325678945",
        adresse: "besorohitra",
        nomMalade: "fatima",
        prenomMalade: "fatima",
        dateNaissance: "2000-02-10",
        cinMalade: "201365987456",
        ficheMedicales: [],
        demandeAides: [{
            idDemandeAide: 3,
            titreDemandeAide: "empute du jambe",
            descriptionDemandeAide: "Nous nous adressons à vous aujourd'hui pour partager l'histoire de fatima, une jeune fille courageuse qui a récemment subi une amputation de la jambe",
            dateDemandeAide: "2024-01-12",
            dateLimite: "2024-02-11",
            lieu: "Province d’Antananarivo, Madagascar",
            longitude: 0,
            latitude: 0,
            statut: false,
            mediaLists: [{
                idMedia: 3,
                fichier: "images/4b5fb5d7-ded9-475e-ab20-d8d161f60336.jpg",
                fileName: "22.jpg",
                typeMedia: "image/jpeg"
            }],
            besoinMaterielLists: [{
                idBesoin: 2,
                estRempli: false,
                intituleBesoinMateriels: "prothèses ",
                quantiteBesoinMateriel: 1
            }],
            besoinFinancierLists: [{
                idBesoin: 2,
                estRempli: false,
                montantDemandee: 1000000
            }],
            donationFinancierLists: [],
            donationMaterielLists: []
        }],
        retours: []
    },
    {
        idUtilisateur: 3,
        mail: "rojoniana@gmail.com",
        motDePasse: "$2a$10$KJSva/Q2NvTycYNXpLWz5.lXWSyWVbGOWidItJCUcON41SrNtuz1C",
        pseudo: "rojoniana",
        contact: "0325678945",
        adresse: "besorohitra",
        nomMalade: "rojniana",
        prenomMalade: "rojoniana",
        dateNaissance: "2001-02-20",
        cinMalade: "20136985478",
        ficheMedicales: [],
        demandeAides: [{
            idDemandeAide: 4,
            titreDemandeAide: " malformation anorectal",
            descriptionDemandeAide: "Rojoniana a le malformation anorectal ,Le trou de son anus est très petit. La saleté ne peut donc pas sortir .",
            dateDemandeAide: "2024-01-12",
            dateLimite: "2024-01-14",
            lieu: "Ambohimiandra, Analamanga, Madagascar",
            longitude: 47.66327,
            latitude: -18.96724,
            statut: false,
            mediaLists: [{
                idMedia: 4,
                fichier: "images/1e71aca5-0cdc-4b63-be96-8ad294f5dd59.jpg",
                fileName: "10.jpg",
                typeMedia: "image/jpeg"
            }],
            besoinMaterielLists: [{
                    idBesoin: 3,
                    estRempli: false,
                    intituleBesoinMateriels: "lait en poudre",
                    quantiteBesoinMateriel: 4
                },
                {
                    idBesoin: 4,
                    estRempli: false,
                    intituleBesoinMateriels: "lait en poudre",
                    quantiteBesoinMateriel: 4
                }
            ],
            besoinFinancierLists: [{
                idBesoin: 3,
                estRempli: false,
                montantDemandee: 4500000
            }],
            donationFinancierLists: [],
            donationMaterielLists: []
        }],
        retours: []
    }
]