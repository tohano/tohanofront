import image from "@/images/aboutMalade.jpg";

const text =
  " Tohano est une plateforme en ligne qui met en relation les malades ou leur famille avec des associations ou des particuliers souhaitant les aider. Les malades peuvent faire des demandes via des publications sur notre plateforme. Ainsi, des donateurs ou des associations pourront apporter leur aide, financièrement ou autrement. Pour y accéder, les malades doivent s’inscrire sur Tohano.";

const textInscription = "Allez à l’onglet Authentification et choisissez \"S’inscrire\". Remplissez le formulaire qui apparaît. L'inscription se fait en trois étapes, cliquez sur Suivant après chaque étape. Cliquez sur \"Valider\" pour confirmer votre inscription. Après l’inscription éffectuée, vous pouvez vous connecter et accéder à tous les services de Tohano.";

const textDonation = "Allez au menu \"Publier une demande d’aide\". Remplissez le formulaire en détaillant vos besoins.";

const textProfil = "Accédez à votre profil. Cliquez sur \"Modifier\". Apportez les modifications nécessaires. Cliquez sur \"Valider\" pour enregistrer vos modifications.";

export const dataMaladeccm = {
  tagline: "",
  title: "Comment demander une aide dans Tohano",
  image,
  text,
  pills: [
    {
      id: "pills-home",
      tagline: "inscription",
      text: textInscription,
    },
    {
      id: "pills-profile",
      tagline: "publier une demande",
      text: textDonation,
    },
    {
      id: "pills-contact",
      tagline: "gérer son profil",
      text: textProfil,
    },
  ],
};