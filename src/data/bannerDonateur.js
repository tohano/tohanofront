import banner3 from "@/images/banner-line.png"
import banner from "@/images/banner-line.png";
import banner1 from "@/images/shape/banner-line-1.png";
import banner2 from "@/images/shape/banner-line-2.png";


export const bannerDonateur = [
  {
    id: 1,
    bg: "bbg1.jpg",
    text: "Donner de l'espoir , changer de vie",
    title: "Tohano Platforms",
    banner,
  },
  {
    id: 2,
    bg: "donateur3.jpg",
    text: "Donateur particulier ou association",
    title: "Offrir vos dons aux nécessiteux",
    banner,
  },
  {
    id: 3,
    bg: "donateur1.jpg",
    title: "Faire un don particulier à tout moment",
    banner: banner3,
    banner1,
    banner2,
  },
  {
    id: 4,
    bg: "donateur4.jpg",
    title: "Nous acceptons tous types de don avec gratitude ",
    banner: banner3,
    banner1,
    banner2,
  },
  {
    id: 5,
    bg: "donateur5.jpg",
    title: "Votre générosité sauve des vies",
  },
  {
    id: 6,
    bg: "donateur2.jpg",
    title: "Ensemble donnons nous la main pour aider nos prochains.",
  },
];
