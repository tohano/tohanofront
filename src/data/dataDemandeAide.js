import author from "@/images/author.jpg";
import image from "@/images/blog-details-thumb.jpg";
import { socials } from "./headerData";

export const dataDemandeAide = {
  tagline: "",
  title: "liste demande d'aide",
  newses: [
    {
      id: 1,
      image: "2.jpg",
      image2: "2.jpg",
      dateDePublication: "3 April, 2020",
      dateLimite:"date limite",
      nomMalade: "mathieu",
      quartier:"quartiet",
      publication: "",
      ville:"ville",
      etat:"valide",
      status:"urgent",
      montantdemande:"",
      typedemande:"materiels",
      descriptif:"chaise roulante",
      quantite:"1",
      etatdeprogression:"",
      title: "tumeur au cerveau",
    },
    {
      id: 2,
      image: "16.jpg",
      image2: "17.jpg",
      dateDePublication: "3 April, 2020",
      dateLimite:"date limite",
      nomMalade: "Andraina",
      quartier:"quartiet",
      publication: "",
      ville:"ville",
      etat:"non valide",
      status:"urgent",
      montantdemande:"1000ar",
      typedemande:"materiels",
      descriptif:"medicament",
      quantite:"1",
      etatdeprogression:"",
      title: "maladie des os",
    },
    {
      id: 3,
      image: "10.jpg",
      image2: "13.jpg",
      dateDePublication: "3 April, 2020",
      dateLimite:"date limite",
      nomMalade: "rojoniana",
      quartier:"quartiet",
      publication: "",
      ville:"ville",
      etat:"valide",
      status:"urgent",
      montantdemande:"1000ar",
      typedemande:"financiere",
      descriptif:"medicament",
      quantite:"",
      etatdeprogression:"",
      title: " malformation anorectal",
    },
    {
      id: 4,
      image: "21.jpg",
      image2: "22.jpg",
      dateDePublication: "3 April, 2020",
      dateLimite:"date limite",
      nomMalade: "fatima",
      quartier:"quartiet",
      publication: "",
      ville:"ville",
      etat:"valide",
      status:"",
      montantdemande:"1000ar",
      typedemande:"financiere",
      descriptif:"",
      quantite:"",
      etatdeprogression:"",
      title: "jambe emputer",
    },
    {
      id: 5,
      image: "26.jpg",
      image2: "26.jpg",
      dateDePublication: "3 April, 2020",
      dateLimite:"date limite",
      nomMalade: "rakoto",
      quartier:"quartiet",
      publication: "Nous lançons donc cet appel à votre générosité pour aider mme Sahondra à couvrir ces dépenses. Toute contribution, quelle qu'elle soit, sera grandement appréciée. Ensemble, nous pouvons faire une différence dans la vie de mme sahondra et lui donner l'espoir et le soutien dont elle a besoin en ce moment.",
      ville:"ville",
      etat:"non valide",
      status:"",
      montantdemande:"1000ar",
      typedemande:"materiels",
      descriptif:"medicament",
      quantite:"",
      etatdeprogression:"",
      title: "ostéogenèse imparfaite",
    },
    {
      id: 6,
      image: "23.jpg",
      image2: "23.jpg",
      dateDePublication: "3 April, 2020",
      dateLimite:"2022-03-25",
      nomMalade: "fanja",
      quartier:"quartiet",
      publication: 2,
      ville:"ville",
      etat:"valide",
      status:"",
      montantdemande:"1000ar",
      typedemande:"materiel",
      descriptif:"medicament",
      quantite:"",
      etatdeprogression:"",
      title: "mucoviscidose",
    },
  ],
};

export const publication = [
  {
    id: 1,
    image: "comment-1-1.jpg",
    name: "Kevin Martin",
    date: "3 March, 2020",
    text: "Nous lançons donc cet appel à votre générosité pour aider mme Sahondra à couvrir ces dépenses. Toute contribution, quelle qu'elle soit, sera grandement appréciée. Ensemble, nous pouvons faire une différence dans la vie de mme sahondra et lui donner l'espoir et le soutien dont elle a besoin en ce moment.",
  },
  {
    id: 2,
    image: "comment-1-2.jpg",
    name: "Sarah albert",
    date: "3 March, 2020",
    text: "Lorem Ipsum is simply dummy text of the rinting and typesetting been the industry standard dummy text ever sincer condimentum purus. In non ex at ligula fringilla lobortis.",
  },
];

export const blogDetailsMain = {
  image,
  date: "3 April, 2022",
  admin: "admin",
  comments: 2,
  title: "A day in the life of entrepreneur & co-founders",
  text1:
    "Aelltes port lacus quis enim var sed efficitur turpis gilla sed sit amet finibus eros. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ndustry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries. Lorem Ipsum is simply dummy text of the new design printng and type setting Ipsum Take a look at our round up of the best shows coming soon to your telly box has been the is industrys. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has industr standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
  text2:
    "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
  tags: ["Construction", "Build", "Factory"],
  socials: socials,
};

export const blogAuthor = {
  image: author,
  name: "Christine Eve",
  text: "Lorem Ipsum is simply dummy text of the rinting and typesetting been the industry standard dummy text ever sincer condimentum purus.",
};

export const blogDetailsSidebar = {
  categories: [
    "Business",
    "Coworking",
    "Freelancers",
    "Creative people",
    "Technology",
    "Startups",
  ],
  tags: [
    "Coworking",
    "Business",
    "Virtual office",
    "Private office",
    "Cabin",
    "Creatives",
    "Office suites",
  ],
  posts: [
    {
      id: 1,
      image: "post-1.jpg",
      title: "A day in the life of entrepreneur & co-founder",
    },
    {
      id: 2,
      image: "post-2.jpg",
      title: "A day in the life of entrepreneur & co-founder",
    },
    {
      id: 3,
      image: "post-3.jpg",
      title: "A day in the life of entrepreneur & co-founder",
    },
  ],
};
