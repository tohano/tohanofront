import Header from "@/components/Header/Header";
import Layout from "@/components/Layout/Layout";
import React from "react";
import DashBoardAdmin from "@/components/DashBoardAdmin/DashBoardAdminContainer";


const AdminHome = () =>{
    return(
        <Layout>
            <Header/>
            <DashBoardAdmin/>
        </Layout>
    )
}

export default AdminHome;