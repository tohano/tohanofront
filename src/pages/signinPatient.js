import Layout from '@/components/Layout/Layout'
import Header from "@/components/Header/Header";
import MaladeForm from "@/components/InscriptionMalade/MaladeForm"
import React from 'react'

const SigninPatient = () => {
    return (
        <Layout>
            <Header />
            <MaladeForm/>
        </Layout>
    )
}

export default SigninPatient