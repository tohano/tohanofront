import AboutIntroduction from "@/components/AboutArea/AboutIntroduction";
import Header from "@/components/Header/Header";
import Layout from "@/components/Layout/Layout";
import PageTitle from "@/components/Reuseable/TitlePatientHome";
import TogetherArea from "@/components/TogetherArea/TogetherArea";
import React from "react";

const About = () => {
  return (
    <Layout>
      <Header />
      <PageTitle title="" parent="pages" />
      <AboutIntroduction />
      <TogetherArea className="together-3-area" />
      
    </Layout>
  );
};

export default About;
