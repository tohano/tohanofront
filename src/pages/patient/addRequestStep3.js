import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Title from '../../components/Reuseable/Title';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { useRouter } from 'next/navigation';
import axios from 'axios';
import Layout from '@/components/Layout/Layout';
import Header from '@/components/Header/Header';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const notify = () => {
        toast.success("Demande d'aide enregistré, nous examinerons votre demande", {
          position: "top-right",
          autoClose: 3500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "colored",
          });
    };

// const numberWithThousandsSeparator = (value) => {
//   return value.replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
// };
function AddRequestStep3(){
    let router = useRouter();
    let idDemande = null;
    if (typeof sessionStorage !== 'undefined') {
        idDemande = JSON.parse(sessionStorage.getItem('idDemandeAide'));
    }
    console.log(idDemande)
    const [besoins, setBesoins] = useState({
        demandeAide:{idDemandeAide:idDemande},
        besoinMateriels: [],
        besoinFinancier: {
        estRempli: false,
        montantDemandee: ''
    }
  });
  const [intituleBesoinMateriel, setIntituleBesoinMateriel] = useState('');
  const [quantiteBesoinMateriel, setQuantiteBesoinMateriel] = useState(0);

  const handleAddBesoinMateriel = (e) => {
    e.preventDefault();
    const newBesoinMateriel = {
      estRempli: false,
      intituleBesoinMateriels: intituleBesoinMateriel,
      quantiteBesoinMateriel: quantiteBesoinMateriel
    };

    setBesoins(prevBesoins => ({
      ...prevBesoins,
      besoinMateriels: [...prevBesoins.besoinMateriels, newBesoinMateriel]
    }));

    setIntituleBesoinMateriel('');
    setQuantiteBesoinMateriel('');
  };

  const handleSetBesoinFinancier = (montantDemandee) => {
    // const vola = (montantDemandee.toString()).replace(/\s/g, '');
    // const drala = numberWithThousandsSeparator(vola);
    setBesoins(prevBesoins => ({
      ...prevBesoins,
      besoinFinancier: {
        estRempli: false,
        montantDemandee: montantDemandee
      }
    }));
  };

  const handleDeleteBesoinMateriel = (index) => {
    setBesoins(prevBesoins => ({
      ...prevBesoins,
      besoinMateriels: prevBesoins.besoinMateriels.filter((_, i) => i !== index)
    }));
  };
//AXIOS POUR ENREGISTRER LES DONNEES A FAIRE
  const validateStep3 = async (e) => {
        e.preventDefault();
        //alert(JSON.stringify(besoins))
        await axios.post("http://localhost:8080/addBesoin",besoins)
            .then(function (response) {
                console.log("Reponse data post besoin io : " + response.data)
                setIntituleBesoinMateriel('');
                setQuantiteBesoinMateriel('');
                notify();
                //setTimeout(()=>{router.push('/maladeHome')},3500)
                //mila amboarina ny push amany am page tokony haleany rehefa vita demande aide
            })
            .catch(function (error){
            console.error("Misy diso besoin ooo " + error)
            
        })
        
    }

    return (
        <Layout>
            <Header/>
            <section className="contact-form-area mt-150">
            <Container>
                <Row className="justify-content-center">
                    <Col lg={8}>
                        <Title title="De quoi avez-vous besoin ?" tagline="Etape 3/3" className="text-center" />
                        <p style={{textAlign:"center", color:"#3DB9CF", marginTop:"-20px"}}>*veuiller spécifier au moins une demande</p>
                    </Col>
                </Row>
                <form>
                    <Row className="justify-content-center">
                <Col lg={8}>
                <div className="conact-form-item">
                    <Row>
                        <Row>
                            <Col className={`input-box mt-20 `} sm={8}>
                                    <h5>Besoin de matériel</h5>
                                    <input
                                        
                                        type="text"
                                        placeholder="Intitulé du matériel"
                                        value={intituleBesoinMateriel}
                                        onChange={(e) => setIntituleBesoinMateriel(e.target.value)}
                                    />
                            </Col>
                            <Col className={`input-box mt-20`} sm={3}>
                                    <h5>Quelle quantité?</h5>
                                        <input
                                
                                            type="number"
                                            placeholder="Quantité"
                                            value={quantiteBesoinMateriel}
                                            onChange={(e) => setQuantiteBesoinMateriel(parseInt(e.target.value))}
                                        />
                            </Col>
                            
                        </Row>
                        <Row>
                            <Col sm={8}>
                                <div className="input-box mt-20 m-20">
                                    <h5 htmlFor="montantDemande">Indiquer nous le montant à collecter</h5>
                                    <input
                                        type="number"
                                        placeholder="Montant demandé"
                                        value={besoins.besoinFinancier.montantDemandee}
                                        onChange={(e) => handleSetBesoinFinancier(parseInt(e.target.value))}
                                    />
                                    
                                    
                                </div>
                            </Col>
                            <Col className='d-flex justify-content-center align-items-center' sm={3}>
                                {
                                    intituleBesoinMateriel.trim() === '' || quantiteBesoinMateriel === 0 || quantiteBesoinMateriel === '' ? (<button  className='button-ajouter' onClick={handleAddBesoinMateriel} disabled>
                                    <AddCircleIcon /> Ajouter
                                </button>) : (<button  className='button-ajouter' onClick={handleAddBesoinMateriel} >
                                    <AddCircleIcon/> Ajouter
                                </button>)
                                }
                                
                            </Col>
                        </Row>
                        <Row className='mt-10' style={{margin:"10px 2px"}}>
                            
                            <table className="table">
                                <caption style={{color: "#F5AC73"}}>Liste des besoins matériels</caption>
                                <thead>
                                <tr>
                                    <th>Intitulé</th>
                                    <th className='cell2'>Quantité</th>
                                    <th className='last-cell'>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {besoins.besoinMateriels.map((besoin, index) => (
                                    <tr key={index}>
                                        <td>{besoin.intituleBesoinMateriels}</td>
                                        <td>{besoin.quantiteBesoinMateriel}</td>
                                        <td>
                                            <IconButton className='action-button' onClick={() => handleDeleteBesoinMateriel(index)}>
                                            <DeleteIcon />
                                            </IconButton>
                                        </td>
                                        
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </Row>
                        <Col lg={12}>
                            <div className="input-box mt-20 text-center">
                            <button className="main-btn main-btn-2 mr-150" >
                                Réinitialiser
                            </button>
                            {/* {validateStep1() ? (
                                
                            ) : (
                                <button className="main-btn" disabled>
                                Valider
                                </button>
                            )} */}
                            <button className="main-btn ml-50" onClick={validateStep3}>
                                Valider
                            </button>
                            </div>
                        </Col>
                    </Row>
                </div>
                <p className="form-message"></p>
                </Col>
          </Row>
                    
                </form>
                  <ToastContainer
                        position="top-right"
                        autoClose={5000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                        theme="colored"
                        />
            </Container>
        </section>
        </Layout>
        
    )
}

export default AddRequestStep3;