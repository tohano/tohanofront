import {React, useState} from "react";

import Layout from "@/components/Layout/Layout";
import Header from "@/components/Header/Header";
import Step1 from "@/components/AddRequestForm/Step1";
import Formulaire from "@/components/AddRequestForm/addRequestForm";

function addRequest(){
    return (
    <Layout>
        <Header/>
        <Step1 />      
        
    </Layout>
    )
}

export default addRequest