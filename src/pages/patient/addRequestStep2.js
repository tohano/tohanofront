import React, {useRef, useState} from "react";
import Layout from "@/components/Layout/Layout";
import Header from "@/components/Header/Header";
import axios from "axios";
import {Button} from "@mui/material";
import {CloudUpload} from "@mui/icons-material";
import { Container, Row, Col } from 'react-bootstrap';
import Title from "@/components/Reuseable/Title";
import { useRouter } from "next/router";


function AddRequestStep2 (){
    const router  = useRouter();
    const [files, setFiles] = useState(null);
    const [progress, setProgress] = useState({started:false, pc:0});
    const [msg, setMsg] = useState("");
    
    const [imageUrls, setImageUrls] = useState([]);
    const fic = useRef([]);
    

    const handleClickUpload = (e)=>{
      e.preventDefault();
        if(!files){
            setMsg("No files selected");
            return;
        }
        const fd = new FormData();
        for(let i=0; i< files.length; i++){
            fd.append(`file`, files[i]);
            fd.append('demandeAide',JSON.parse(sessionStorage.getItem('idDemandeAide')));
            console.log(fd);
        }
        setMsg("Uploading,....");

        setProgress(prevState=>{
            return {...prevState, started:true}
        })

        axios.post('http://localhost:8080/files/multi_uploadfile', fd, {
            onUploadProgress:(ProgressEvent)=>{
                setProgress(prevState =>{
                    return {... prevState, pc : ProgressEvent.progress*100}
                })
            },
            headers:{
                "Custom-Header":"value",
            }
        })
        .then(res=>{
            console.log(res.data);
            setMsg("Ajout média réussi");
            router.push("/patient/addRequestStep3")

        })
        .catch(err =>{
            console.log(err)
            setMsg("Ajout média échoué");
        });



    }
    const onChangeHandle = (event)=>{
        console.log(fic.current.files);
        const urls = [];
        const images =[];

        const filess = fic.current.files;

        for (let i = 0; i < filess.length; i++) {
            const file = filess[i];
            const imageUrl = URL.createObjectURL(file);
            urls.push(imageUrl);
            images.push(file);
        }

        setImageUrls(urls);
        setFiles(images);

    }
    return (
        <Layout>
            <Header/>
            <section className="contact-form-area mt-150">
        <Container>
          <form  >
          <Row className="justify-content-center">
            <Col lg={8}>
              <Title title="Ajouter des médias pour appuyer votre demande" tagline="Etape 2/3" className="text-center" />
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col lg={8}>
              <div className="conact-form-item">
                <Row>
                  <Row>
                    {progress.started && <progress max="100" value={progress.pc}></progress>}
                    {msg && <span>{msg}</span>}
                  </Row>
                  <Col lg={12}>
                    <div className="input-box mt-20 text-center">
                        <Button  component="label" variant="contained" startIcon={<CloudUpload />}>
                            Importer images
                            <input type="file" ref={fic} onChange={onChangeHandle}  multiple hidden/>
                        </Button>
                      
                      {
                        imageUrls.length !== 0 ? (<button className="main-btn ml-150" onClick={handleClickUpload}>
                            Valider
                        </button>) : ""
                      }
                        
                      
                    </div>
                  </Col>
                </Row>
              </div>
              <Row>
                
                <p className="form-message"></p>
              </Row>
                
            </Col>
          </Row>
          </form>
        </Container>
        <div style={{margin:50, display:"flex", justifyContent:"center"}}>
        <div style={{marginBottom:300, display:"flex", flexWrap:"wrap", gap:20}}>
                    {imageUrls.map((url, index) => (
                        <img key={index} src={url} alt={`Image ${index}`} style={{ width: '500px', height:'500px',
                        objectFit:"cover",  marginTop: '10px', display:"flex", flexWrap:"wrap" }} />
                        ))}
                </div>
        </div>
      </section>
      


        </Layout>
    )
}

export default AddRequestStep2;