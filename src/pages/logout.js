import LogoutForm from "@/components/LogoutForm/LogoutForm";
import { useRouter } from "next/router";
import React, { useEffect } from "react";



const Logout = () => {
    
    const route = useRouter();
    
    useEffect(() => {
        sessionStorage.clear();
        route.push("/");
    }, []);

    return (
        // <LogoutForm />
        console.log("deconnected")
    )
}

export default Logout;

