import UserAuthenticationForm from "@/components/UserAuthenticationForm/UserAuthenticationForm";
import Header from "@/components/Header/Header";
import Layout from "@/components/Layout/Layout";
import React from "react";

const Login = () => {

    return (
        <Layout>
            <Header />
            <UserAuthenticationForm />
        </Layout>
    )
}

export default Login;