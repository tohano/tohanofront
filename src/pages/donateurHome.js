import React from "react";
import Link from "next/link";
import Layout from "@/components/Layout/Layout";
import Header from "@/components/Header/Header";
import BannerSliderDonateur from "@/components/BannerSlider/BannerSliderDonateur";
import Howdonateur from "@/components/Donateur/Howdonateur";
import CtaAreaDonateur from "@/components/CtaArea/CtaAreaDonateur";
import FactureComponent from "@/components/Facture/LIsteDonateur";
import Example from "@/components/Facture/Exemplemodal";


function DonateurHome (){
    return (
        <Layout>
            <Header />
            <BannerSliderDonateur />
            <Howdonateur/>
            <CtaAreaDonateur/>
           
           <Example/>
           

            
        </Layout>
    )
}

export default DonateurHome;