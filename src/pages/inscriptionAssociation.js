import Layout  from "@/components/Layout/Layout";
import Header from "@/components/Header/Header";
import AssociationFormulaire  from "@/components/InscriptionDonateur/InscriptionAssociation/AssociationFormulaire";

import React from 'react'

const inscrireAssociation = () => {
    return (
        <Layout>
            <Header />
            <AssociationFormulaire />
        </Layout>
    )
}

export default inscrireAssociation

