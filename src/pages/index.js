import BannerSlider from "@/components/BannerSlider/BannerSlider";
import Categories from "@/components/Categories/Categories";
import Header from "@/components/Header/Header";
import Layout from "@/components/Layout/Layout";
import ProjectsArea from "@/components/ProjectsArea/ProjectsArea";
import React from "react";
import EasyStepsArea from "@/components/EasyStepsArea/EasyStepsArea";
import UrgentHelpRequestsArea from "@/components/UrgentHelpRequestsArea/UrgentHelpRequestsArea";


const Home = () => {
  return (
    <Layout>
      <Header />
      <BannerSlider />
      <Categories />
      <UrgentHelpRequestsArea />
      {/*<CtaArea /> */}
      <ProjectsArea />
      <EasyStepsArea/>
      
    </Layout>
  );
};

export default Home;
