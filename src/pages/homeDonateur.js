
import BannerSliderDonateur from "@/components/BannerSlider/BannerSliderDonateur";
import CtaArea from "@/components/CtaArea/CtaArea";
import CtaAreaDonateur from "@/components/CtaArea/CtaAreaDonateur";
import Header from "@/components/Header/Header";
import Layout from "@/components/Layout/Layout";
import Howdonateur from "@/components/Donateur/Howdonateur";
import React from "react";

const HomeDonateur = () => {
  return (
    <Layout>
      <Header />
      <BannerSliderDonateur />
      <Howdonateur/>
      <CtaAreaDonateur/>
    </Layout>
  );
};

export default HomeDonateur;
