import DataTable from '@/components/datatable/DataTable';
import React from 'react';

const columns = [
    { field: "id", headerName: "ID", width: 60 },
    {
      field: "img",
      headerName: "Image",
      width: 70,
      renderCell: (params) => {
        return <img src={params.row.img || "/assets/noavatar.png"} alt="" />;
      },
    },
    {
      field: "title",
      type: "string",
      headerName: "Title",
      width: 225,
    },
    {
      field: "color",
      type: "string",
      headerName: "Color",
      width: 100,
    },
    {
      field: "producer",
      type: "string",
      headerName: "Producer",
      width: 200,
    },
    {
      field: "price",
      type: "string",
      headerName: "Price",
      width: 150,
    },
    {
      field: "createdAt",
      headerName: "Created At",
      width: 150,
      type: "string",
    },
    {
      field: "inStock",
      headerName: "In Stock",
      width: 105,
      type: "boolean",
    },
  ];

const test = () => {
    return (
        <div>
            <DataTable columns={columns}/>
        </div>
    )
}

export default test;