import Header from '@/components/Header/Header'
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import CheckIcon from '@mui/icons-material/Check';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import StripeCheckout from 'react-stripe-checkout';


 const donation = () => {
    const [listeDemande,setListDemande] = useState([]);

    const handleToken= () => {

    }

    useEffect( () =>{
        const fetchList = async () => {
            try{
                let data = await axios.get("http://localhost:8080/demandeaide/list");
                //console.log("AZo ny data : ",data.data)
                let reponse = data.data;
                setListDemande(reponse);
            }catch(error){
                console.log("Erreur ato am maka data admin *****" + error)
            }
        };
        fetchList();
        
    },[]);
    console.log(listeDemande);
    return (
       <>
        <Header/>
       
        <div className="panel panel-default mt-150 ml-100">
                <div className="panel-heading wt-panel-heading p-a20 mb-20">
                    <h4 className="panel-tittle m-a0">
                        <FormatListBulletedIcon/> Listes des demandes d'aides
                    </h4>
                </div>
                <div className="panel-body wt-panel-body p-a20 m-b30 ">
                    <div className="twm-D_table p-a20 table-responsive">
                        <table className="table table-bordered twm-bookmark-list-wrap">
                            <thead>
                                <tr>
                                    <th>Intitulé demande aide</th>
                                    <th>Date création</th>
                                    <th>Date limite</th>
                                    <th>Montant demandé</th>
                                    <th>Récolté</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                   listeDemande !== undefined && Array.isArray(listeDemande) ? 
                                   (listeDemande.map((demande,index)=>{
                                        return(
                                        <tr key ={index} >
                                            <td>{demande.titreDemandeAide}</td>
                                            <td>{demande.dateDemandeAide}</td>
                                            <td>{demande.dateLimite}</td>
                                            <td>10 000 Ar</td>
                                            <td>{demande.statut ? (<CheckIcon/>) : (<RemoveCircleIcon style={{color:"red"}}/>) }</td>
                                            <td>
                                                <StripeCheckout
                                                    stripeKey="pk_test_51OXPCnCUN88RfCuSHATZ7kgnsicWWIDM8HXWOmHrySMoghmR2JS4vtc6mTqfD5V1mbNXDVfldkJy5HNeKLmQ7sj0000TRGOMvD"
                                                    token={handleToken} // Utilisez la fonction handleToken
                                                    currency="MGA"
                                                    name="Transfert suite donation"
                                                    description="Paiement par carte de crédit"
                                                
                                                >
                                                    <button style={{backgroundColor:"#F5AC73",color:"white",border:"none", width:"70px", }}   onMouseEnter={(e) => {
                                                    e.target.style.backgroundColor = "#3DB9CF";
                                                        }}
                                                        onMouseLeave={(e) => {
                                                            e.target.style.backgroundColor = "#F5AC73";
                                                        }}>
                                                        Payer
                                                    </button>
                                                </StripeCheckout>
                                            </td>
                                        </tr>)
                                        
                                    })) 
                                    
                                    : (<tr>
                                        <p>Pas de données à afficher</p>
                                    </tr>)
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
       </>
    )
}

export default donation;