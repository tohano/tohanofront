import MapDemande from "@/components/Map/mapDemandeAide";
import Header from "@/components/Header/Header";
import { useState, useEffect } from "react";
import axios from "axios";
import SearchIcon from '@mui/icons-material/Search';
// https://maps.geoapify.com/v1/tile/osm-bright/{z}/{x}/{y}.png?apiKey=1b6e35d783254eaf9c85a0deab00d9e2

const DemandeAide = () => {
  const [coords, setCoords] = useState([-18.8974754, 47.54294]);
  const [marquers, setMarquers] = useState([]);
  const [inputSearch, setInputSearch] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get("http://localhost:8080/getvalidatedemandeaide");
        setMarquers(response.data);
      } catch (error) {
        console.log("Erreur:", error);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="demande-aide">
      <Header />
      
      <div className="map">
        <MapDemande coords={coords} markers={marquers}/>
        <div className="guide">
          <p>Localisation des malades qui ont besoin d'aide</p>
        </div>
        <div className="menu-map">
          <SearchIcon className="search-icon" />
          <input type="text" placeholder="Rechercher les infos sur les malades"
              onChange={(e) => {
              console.log(e.target.value);
              setInputSearch(e.target.value);
              }}/>
          <div className="container-pub">
            <ul className="list-pubs">
              {marquers
                .filter((marquer) => {
                  return inputSearch.toLowerCase() === ""
                    ? marquer
                    : marquer.lieu.toLowerCase().includes(inputSearch.toLowerCase()) 
                        || marquer.descriptionDemandeAide.toLowerCase().includes(inputSearch.toLowerCase()) 
                        || marquer.titreDemandeAide.toLowerCase().includes(inputSearch.toLowerCase());
                })
                .map((marquer, indx) => {
                  return (
                    <li className="pub" key={indx} onClick={()=>{
                        setCoords([marquer.latitude, marquer.longitude]);
                        }}>
                        <div className="apropos">
                          <h4>{marquer.titreDemandeAide}</h4>
                          <span>{marquer.lieu}</span>
                        </div>
                        {/* <div className="ville">
                          <span>
                            
                          </span>
                        </div> */}
                    </li>
                  );
                })}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DemandeAide;