import React from 'react'
import Header from '@/components/Header/Header'
import PageTitle from '@/components/Reuseable/PageTitle'
import Layout from '@/components/Layout/Layout'

import { Titlehowtowork } from '@/components/HowtoWork/Titlehowtowork'
import SectionDonateur from '@/components/HowtoWork/SectionDonateur'
import SectionMalade from '@/components/HowtoWork/SectionMalade'

const howtowork=()=> {
  return (
    <Layout>
      <Header />
      <Titlehowtowork/>
      <SectionDonateur/>
      <SectionMalade/>
      
      
    </Layout>
  )
}

export default howtowork