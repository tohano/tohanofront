

import axios from "axios";
import React, { useState } from "react";
import { Col, Row } from "react-bootstrap";
import StripeCheckout from "react-stripe-checkout";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


const PaiementForm = (idDemande) => {
  
  const [inputValue, setInputValue] = useState('');


  const handleInputChange = (event) => {
    setInputValue(event.target.value);
  };

 console.log(inputValue)

  const handleToken = async (token) => {
    

    //alert(token.id);
    //creer une donation, MILA DYNAMISENA MIANDRY AN DINASOA NY VALEUR
    const donation = {
        "montantAideFinancier": inputValue,
        "raisonDonationFinancier": "",
        "etatPaiement": "en attente",
        "demandeAide": {
            "idDemandeAide":idDemande.idDemande
        },
        "donateurAssociation":{
            "idUtilisateur":1
        }
    }
    const donationMax = await axios.get(`http://localhost:8080/donation/getall/${donation.demandeAide.idDemandeAide}`)
    //appel de l'endpoint, mila calculena ny max azo ampidirina 
    
        await axios.post(`http://localhost:8080/donate/${donation.demandeAide.idDemandeAide}/${donation.donateurAssociation.idUtilisateur}`,
              donation,
              { headers: 
                { 'Content-Type': 'application/json' },
                idStripe: token.id
              })
            .then((response)=>{
              toast.success(
                "Paiement accepté, nous informerons le bénéficaire. Nous vous remercions pour votre contribution", {
                  position: "top-right",
                  autoClose: 3000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                  theme: "colored",
                  });
            })
            .catch((err)=>{
                  console.log(err),
                  toast.error("Une erreur est survenue lors du paiement");
                })
  };

      
  return (
    <>
      <Row className="stripeForm">
          <input
              type="text"
              value={inputValue}
              onChange={handleInputChange}
            />
      </Row>

      <Row className="stripeForm">
        <Col>
        </Col>
        <Col className="mt-10 ">
          <StripeCheckout
              stripeKey="pk_test_51OXPCnCUN88RfCuSHATZ7kgnsicWWIDM8HXWOmHrySMoghmR2JS4vtc6mTqfD5V1mbNXDVfldkJy5HNeKLmQ7sj0000TRGOMvD"
              token={handleToken} // Utilisez la fonction handleToken
              currency="MGA"
              name="Donation sur Tohano"
              description="Paiement par carte de crédit"
              
              className="nav-link">

                <button className="main-btn">Payer par carte</button>

              </StripeCheckout>
        </Col>
              
      </Row>

      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
        />
          
    </>
  );

}
export default PaiementForm;