import Header from "@/components/Header/Header";
import Layout from "@/components/Layout/Layout";
import ProjectDetailsArea from "@/components/ProjectsArea/ProjectDetails/ProjectDetailsArea";
import ProjectDetailsContent from "@/components/ProjectsArea/ProjectDetails/ProjectDetailsContent";
import { useRouter } from "next/router";
import React, { createContext, useEffect, useRef, useState } from "react";

// const DetailsDemandeContext = createContext();

// const DetailsProvider = ({children}) => {
//   const router = useRouter();

//   useEffect(() => {
//     setQueryId(router.query.id || '');
//   }, [router.query.id]);

//   return (
//     <DetailsDemandeContext.Provider value={queryId}>
//       {children}
//     </DetailsDemandeContext.Provider>
//   )
// }

const DetailsDemandeAide = () => {

  const router = useRouter();
  const idUrl = router.query.id;
 // console.log("idUrl :::::::::::::::::" + idUrl + "idUrl == undefined ==>" + (idUrl==undefined));
  const [queryId, setQueryId] = useState('');

 
  useEffect(() => {
    setQueryId(router.query.id || '');
  }, [router.query.id]);
  
  // console.log("Ilay azo avy amin'ny URL ===================>" + queryId);
  // console.log(demandeAide);
  
    return (
      <Layout>
        <Header />
        {/* <PageTitle title="Aide pour un traitement post-opératoire" page="Explore" /> */}
        <ProjectDetailsArea idDemande={queryId}/>
        <ProjectDetailsContent idDemande={queryId}/>
        {/* <SimilarProjects /> */}
      </Layout>
    );
};

export default DetailsDemandeAide;
