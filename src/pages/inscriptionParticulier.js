import Layout from "@/components/Layout/Layout";
import Header from "@/components/Header/Header";
import React from 'react'
import ParticulierFormulaire from "@/components/InscriptionDonateur/InscriptionParticulier/ParticulierFormulaire";

const InscrireAssociation = () => {
    return (
        <Layout>
            <Header />
            <ParticulierFormulaire />
        </Layout>
    )
}
export default InscrireAssociation
